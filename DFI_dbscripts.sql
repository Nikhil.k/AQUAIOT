
CREATE DATABASE  IF NOT EXISTS `DFIdev`;

USE `DFIdev`;



-- Countrymst

DROP TABLE IF EXISTS `countrymst`;

CREATE TABLE `countrymst` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Stores all countries',
  `countryname` varchar(45) DEFAULT NULL,
  `countrycd` varchar(45) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


-- Statemst


DROP TABLE IF EXISTS `statemst`;

CREATE TABLE `statemst` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'Stores all states',
  `statecd` varchar(45) DEFAULT NULL,
  `statename` varchar(45) DEFAULT NULL,
  `countrymstid` bigint(11) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- Districtmst

DROP TABLE IF EXISTS `districtmst`;


CREATE TABLE `districtmst` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `districtcd` varchar(45) DEFAULT NULL,
  `districtame` varchar(45) DEFAULT NULL,
  `statemstid` varchar(45) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Citymst

DROP TABLE IF EXISTS `citymst`;


CREATE TABLE `citymst` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `citycd` varchar(45) DEFAULT NULL,
  `cityname` varchar(45) DEFAULT NULL,
  `districtmstid` bigint(11) DEFAULT NULL,
  `statemstid` bigint(11) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `updatedBy` varchar(45) DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3584 DEFAULT CHARSET=utf8;


-- DFIlookupmst

DROP TABLE IF EXISTS `dfilookupmst`;

CREATE TABLE `dfilookupmst` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `valuecode` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `ParentId` bigint(11) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `createdby` varchar(45) DEFAULT NULL,
  `createdAt` date DEFAULT NULL,
  `modififedby` varchar(45) DEFAULT NULL,
  `updatedAt` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- ------------------------------------- ORGANIZATION DETAILS ----------------------------------------------------
-- Address master table

DROP TABLE IF EXISTS `address`;

CREATE TABLE `address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Address1` VARCHAR(512) NULL,
  `Address2` VARCHAR(512) NULL,
  `countrymstid` BIGINT(10) NULL,
  `statemstid` BIGINT(10) NULL,
  `districtmstid` BIGINT(10) NULL,
  `citymstid` BIGINT(10) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
  
-- Zonemst

DROP TABLE IF EXISTS `zonemst`;

CREATE TABLE `zonemst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `zonename` VARCHAR(45) NULL,
  `zonecode` VARCHAR(45) NULL,
  `zonecoordinater` VARCHAR(45) NULL,
  `addressid` BIGINT(11) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL, 
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- Branchmst

DROP TABLE IF EXISTS `branchmst`;
	
CREATE TABLE `branchmst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `branchname` VARCHAR(45) NULL,
  `branchcode` VARCHAR(45) NULL,
  `addressid` BIGINT(12) NULL,
  `zonemstid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
-- Department

DROP TABLE IF EXISTS `department`;

  CREATE TABLE `department` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `deptname` VARCHAR(45) NULL,
  `deptcode` VARCHAR(45) NULL,
  `addressid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  -- Bankdtls

DROP TABLE IF EXISTS `bankdtls`;


CREATE TABLE `bankdtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bankname` VARCHAR(45) NULL,
  `accountnum` VARCHAR(45) NULL,
  `branchname` VARCHAR(45) NULL,
  `ifsccode` VARCHAR(45) NULL,
  `accountholdername` VARCHAR(45) NULL,
  `isactive` VARCHAR(1) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- Legalentitydtls

DROP TABLE IF EXISTS `legalentitydtls`;

  CREATE TABLE `legalentitydtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `organizationtype` VARCHAR(45) NULL,
  `organizationcode` VARCHAR(45) NULL,
  `countrymstid` BIGINT(12) NULL,
  `statemstid` BIGINT(12) NULL,
  `districtmstid` BIGINT(12) NULL,
  `citymstid` BIGINT(12) NULL,
  `addressid` BIGINT(12) NULL,
  `bankdtlsid` BIGINT(12) NULL,
  `contactnum` BIGINT(12) NULL,
  `faxnumber` BIGINT(12) NULL,
  `website` VARCHAR(125) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
-- Responsibilties

DROP TABLE IF EXISTS `responsibilities`;
 
CREATE TABLE `responsibilities` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `respname` VARCHAR(45) NULL,
  `respcode` VARCHAR(45) NULL,
  `parentid` BIGINT(12) NULL,
  `displayname` VARCHAR(45) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
 
-- Rolemst

DROP TABLE IF EXISTS `rolemst`;

CREATE TABLE `rolemst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rolenm` VARCHAR(45) NULL,
  `rolemstid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- Roleresponsibilities

DROP TABLE IF EXISTS `roleresponsibilities`;

CREATE TABLE `roleresponsibilities` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rolemstid` BIGINT(12) NULL,
  `responsibilitiesid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));


-- Employee

DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(45) NULL,
  `middlename` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `gender` VARCHAR(1) NULL,
  `dateofbirth` DATE NULL,
  `dateofjoining` DATE NULL,
  `emailid` VARCHAR(45) NULL,
  `mobilenum` BIGINT(12) NULL,
  `bloodgroup` VARCHAR(10) NULL,
  `emergencycontactnum` BIGINT(12) NULL,
  `aadharcardnum` VARCHAR(45) NULL,
  `pancard` VARCHAR(45) NULL,
  `photourl` VARCHAR(70) NULL,
  `paddressid` BIGINT(12) NULL,
  `caddressid` BIGINT(12) NULL,
  `bankdtlsid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
  
-- Memeber

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `branchmstid` VARCHAR(45) NULL,
  `receiptid` VARCHAR(45) NULL,
  `receiptdate` DATE NULL,
  `typeofmembership` VARCHAR(45) NULL,
  `photo` BLOB NULL,
  `surname` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `gouthram` VARCHAR(45) NULL,
  `fathersname` VARCHAR(45) NULL,
  `husbandname` VARCHAR(45) NULL,
  `mothersname` VARCHAR(45) NULL,
  `maritalstatus` VARCHAR(45) NULL,
  `gender` VARCHAR(1) NULL,
  `Isheperformingsandhyavandhanam` VARCHAR(1) NULL,
  `isdoingpooja` VARCHAR(1) NULL,
  `reason` VARCHAR(512) NULL,
  `dateofbirth` DATE NULL,
  `age` BIGINT(12) NULL,
  `bloodgroup` VARCHAR(45) NULL,
  `educationalcategory` VARCHAR(45) NULL,
  `educationalqualification` VARCHAR(45) NULL,
  `occupation` VARCHAR(45) NULL,
  `professionaldetails` VARCHAR(45) NULL,
  `typeoforganization` VARCHAR(45) NULL,
  `nameoforganization` VARCHAR(45) NULL,
  `designation` VARCHAR(45) NULL,
  `officeaddress` VARCHAR(125) NULL,
  `styleofnametobedisplayedoncertificates` VARCHAR(45) NULL,
  `ParticularsofPostRetirementEmploymentifany` VARCHAR(125) NULL,
  `paddressid` BIGINT(12) NULL,
  `caddressid` BIGINT(12) NULL,
  `mobilenum` BIGINT(20) NULL,
  `alternatemobilenum` BIGINT(20) NULL,
  `emailid` VARCHAR(45) NULL,
  `website` VARCHAR(105) NULL,
  `voterid` VARCHAR(45) NULL,
  `pancardno` VARCHAR(45) NULL,
  `rationcardno` VARCHAR(45) NULL,
  `passportno` VARCHAR(45) NULL,
  `aadharcardno` VARCHAR(45) NULL,
  `istdaapplicable` VARCHAR(1) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- MemberBankdtls

DROP TABLE IF EXISTS `memberbankdtls`;

CREATE TABLE `memberbankdtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `memberid` BIGINT(12) NULL,
  `bankdtlsid` BIGINT(12) NULL,
  `isactive` VARCHAR(1) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));


  
-- MemberAdditionalParticular's

DROP TABLE IF EXISTS `memberadditionalparticulars`;

CREATE TABLE `memberadditionalparticulars` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `memberid` BIGINT(12) NULL,
  `village` VARCHAR(45) NULL,
  `mandal` VARCHAR(45) NULL,
  `statemstid` BIGINT(12) NULL,
  `districtmstid` BIGINT(12) NULL,
  `introducedtodfiby` VARCHAR(45) NULL,
  `ismarried` VARCHAR(1) NULL,
  `nameandaddressoffather-in-law` VARCHAR(512) NULL,
  `gothramoffather-in-law` VARCHAR(45) NULL,
  `nameofmother-in-law` VARCHAR(45) NULL,
  `nameandaddressoffather` VARCHAR(45) NULL,
  `gothramoffather` VARCHAR(45) NULL,
  `iscontactreference` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `mobilenum` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
-- MemberElectrolInformation


DROP TABLE IF EXISTS `memberelectrolinfomation`;


CREATE TABLE `memberelectrolinfomation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `memberid` BIGINT(12) NULL,
  `villagepanchayat` VARCHAR(45) NULL,
  `MPP` VARCHAR(45) NULL,
  `ZPP` VARCHAR(45) NULL,
  `wardmember` VARCHAR(45) NULL,
  `muncipality` VARCHAR(45) NULL,
  `corporation` VARCHAR(45) NULL,
  `MLAconstituency` VARCHAR(45) NULL,
  `MLCconstituency` VARCHAR(45) NULL,
  `MPconstituency` VARCHAR(45) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

  
-- Dispatchdtls

DROP TABLE IF EXISTS `dispatchdtls`;

CREATE TABLE `dispatchdtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `courierdispatchnum` BIGINT(20) NULL,
  `couriername` VARCHAR(45) NULL,
  `couriercontactnum` BIGINT(20) NULL,
  `deliveryaddress` VARCHAR(125) NULL,
  `memberid` BIGINT(12) NULL,
  `otherid` BIGINT(12) NULL,
  `description` VARCHAR(512) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

  
-- Receipt

DROP TABLE IF EXISTS `receipt`;


CREATE TABLE `receipt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `reciepttype` VARCHAR(45) NULL,
  `memberid` BIGINT(12) NULL,
  `receiptdate` DATE NULL,
  `mobilenum` BIGINT(12) NULL,
  `amount` VARCHAR(45) NULL,
  `paymentmode` VARCHAR(45) NULL,
  `chequenum` VARCHAR(45) NULL,
  `chequedate` DATE NULL,
  `bankname` VARCHAR(45) NULL,
  `transactionid` BIGINT(12) NULL,
  `towards` VARCHAR(45) NULL,
  `receivedby` VARCHAR(45) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- Familydtls

DROP TABLE IF EXISTS `familydtls`;

CREATE TABLE `familydtls` (
  `id` INT NOT NULL  AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `relationship` VARCHAR(45) NULL,
  `dateofbirth` DATE NULL,
  `gender` VARCHAR(1) NULL,
  `qualification` VARCHAR(512) NULL,
  `mobilenum` BIGINT(20) NULL,
  `occupation` VARCHAR(512) NULL,
  `addressid` BIGINT(11) NULL,
  `employeeid` BIGINT(11) NULL,
  `memberid` BIGINT(11) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updatedby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
 
-- Employeelegalentity

DROP TABLE IF EXISTS `employeelegalentity`;

CREATE TABLE `employeelegalentity` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `employeeid` BIGINT(12) NULL,
  `legalentitydtlsid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
-- Employeeeducationaldtls


DROP TABLE IF EXISTS `employeeeducationaldtls`;


CREATE TABLE `employeeeducationaldtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qualificationname` VARCHAR(45) NULL,
  `yearofpassing` BIGINT(12) NULL,
  `percentage` BIGINT(12) NULL,
  `employeeid` BIGINT(12) NULL,
  `university` VARCHAR(45) NULL,
  `college` VARCHAR(45) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- Employeeexperience

DROP TABLE IF EXISTS `employeeexperience`;

CREATE TABLE `employeeexperience` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `companyname` VARCHAR(105) NULL,
  `designation` VARCHAR(45) NULL,
  `from` DATE NULL,
  `to` DATE NULL,
  `lastsalarydrawn` BIGINT(20) NULL,
  `employeeid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- Employeerole


DROP TABLE IF EXISTS `employeerole`;

CREATE TABLE `employeerole` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `reportingmangerid` BIGINT(12) NULL,
  `departmentid` BIGINT(12) NULL,
  `rolemstid` BIGINT(12) NULL,
  `zonemstid` BIGINT(12) NULL,
  `branchmstid` BIGINT(12) NULL,
  `employeeid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
-- UserMst

DROP TABLE IF EXISTS `usermst`;

CREATE TABLE `usermst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `memberid` VARCHAR(45) NULL,
  `loginid` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  `employeeid` VARCHAR(45) NULL,
  `userstatus` VARCHAR(45) NULL,
  `oldpassword` VARCHAR(45) NULL,
  `lastlogging` VARCHAR(45) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

--  ---------------------------------------  INVESTMENTS ---------------------------------------------------------


-- Investmst

DROP TABLE IF EXISTS `investmentmst`;

CREATE TABLE `investmentmst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `investmenttype` VARCHAR(45) NULL,
  `investmentcode` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- Investmentlockingmst

DROP TABLE IF EXISTS `investmentlockingmst`;


CREATE TABLE `dfidev`.`investmentlockingmst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `investmentmstid` BIGINT(12) NULL,
  `lockingperiod` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- Frequencymst

DROP TABLE IF EXISTS `frequencymst`;

CREATE TABLE `frequencymst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `frequencytype` VARCHAR(45) NULL,
  `frequencyname` VARCHAR(45) NULL,
  `frequencycode` VARCHAR(45) NULL,
  `description` VARCHAR(125) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- Intrestrateslabmst

DROP TABLE IF EXISTS `intrestrateslabmst`;

  CREATE TABLE `intrestrateslabmst` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rateofinterest` VARCHAR(45) NULL,
  `displayname` VARCHAR(45) NULL,
  `description` VARCHAR(125) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- InvestmentApplication

DROP TABLE IF EXISTS `investmentapplication`;


CREATE TABLE `investmentapplication` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `investmentmstid` BIGINT(12) NULL,
  `frequencymstid` BIGINT(12) NULL,
  `intrestrateslabmstid` BIGINT(12) NULL,
  `principleamount` BIGINT(12) NULL,
  `maturitydate` DATE NULL,
  `memberid` BIGINT(12) NULL,
  `investmentlockingmstid` BIGINT(12) NULL,
  `companyname` VARCHAR(45) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

  
-- RemittanceByInvetsor

DROP TABLE IF EXISTS `remittancebyinvetsor`;

CREATE TABLE `remittancebyinvetsor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `receiptid` BIGINT(12) NULL,
  `amount` BIGINT(12) NULL,
  `investmentapplicationid` BIGINT(12) NULL,
  `memberid` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- Nomineedtls

DROP TABLE IF EXISTS `nomineedtls`;


CREATE TABLE `nomineedtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `relationship` VARCHAR(45) NULL,
  `dateofbirth` DATE NULL,
  `gender` VARCHAR(1) NULL,
  `qualification` VARCHAR(45) NULL,
  `mobilenum` BIGINT(12) NULL,
  `occupation` VARCHAR(45) NULL,
  `address` VARCHAR(125) NULL,
  `memberid` VARCHAR(45) NULL,
  `employeeid` VARCHAR(45) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- InvestmentBonddtls

DROP TABLE IF EXISTS `investmentbonddtls`;

CREATE TABLE `investmentbonddtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `investmentapplicationid` BIGINT(12) NULL,
  `investmentmstid` BIGINT(12) NULL,
  `frequencymstid` BIGINT(12) NULL,
  `intrestrateslabmstid` BIGINT(12) NULL,
  `investmentlockingmstid` BIGINT(12) NULL,
  `memberid` BIGINT(12) NULL,
  `nomineedtlsid` BIGINT(12) NULL,
  `principleamount` BIGINT(12) NULL,
  `maturitydate` DATE NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- BondApprovaldtls

DROP TABLE IF EXISTS `bondapprovaldtls`;

CREATE TABLE `bondapprovaldtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `investmentbonddtlsid` BIGINT(12) NULL,
  `remarks` VARCHAR(512) NULL,
  `approverid` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- -------------------------------------------------- PAYMENTS ---------------------------------------------------------------

-- PaymentBatch

DROP TABLE IF EXISTS `paymentbatch`;

CREATE TABLE `paymentbatch` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `batchname` VARCHAR(45) NULL,
  `batchdate` DATE NULL,
  `totalnumberofbonds` BIGINT(12) NULL,
  `investmentmstid` BIGINT(12) NULL,
  `frequencymstid` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- PayrollBatchdtls

DROP TABLE IF EXISTS `paymentbatchdtls`;


CREATE TABLE `paymentbatchdtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `paymentbatchid` BIGINT(12) NULL,
  `investmentbonddtls` BIGINT(12) NULL,
  `interestamount` DECIMAL(16,8) NULL,
  `principleamount` BIGINT(12) NULL,
  `rateofinterest` DECIMAL(16,8) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- MemberPayment

DROP TABLE IF EXISTS `memberpayment`;

CREATE TABLE `memberpayment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `paymentbatchdtlsid` BIGINT(12) NULL,
  `paymentamount` DECIMAL(16,8) NULL,
  `TDSamount` DECIMAL(16,8) NULL,
  `financialyear` VARCHAR(45) NULL,
  `paymentstartdate` DATE NULL,
  `paymentenddate` DATE NULL,
  `totalpaiddays` BIGINT(12) NULL,
  `totalpayableamount` DECIMAL(16,8) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- InitiateBenificiaryPayment

DROP TABLE IF EXISTS `initiatebenificiarypayment`;

CREATE TABLE `initiatebenificiarypayment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `paymentbatchid` BIGINT(12) NULL,
  `raisedby` BIGINT(12) NULL,
  `raiseddate` DATE NULL,
  `modeofpayment` VARCHAR(45) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- PaymentApproval

DROP TABLE IF EXISTS `paymentapproval`;

CREATE TABLE `paymentapproval` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `initiatebenificiarypaymentid` BIGINT(12) NULL,
  `amount` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- PaymentApprovalRemarks

DROP TABLE IF EXISTS `paymentapprovalremarks`;

CREATE TABLE `paymentapprovalremarks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `paymentapprovalid` BIGINT(12) NULL,
  `approverid` BIGINT(12) NULL,
  `remarks` VARCHAR(125) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));


-- PaymentTransfer

DROP TABLE IF EXISTS `paymenttransfer`;


CREATE TABLE `paymenttransfer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `paymentbatchid` BIGINT(12) NULL,
  `initiatebenificiarypaymentid` BIGINT(12) NULL,
  `chequenum` BIGINT(12) NULL,
  `bankdtlsid` BIGINT(12) NULL,
  `amount` DECIMAL(16,8) NULL,
  `transactionid` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));


-- --------------------------------------------------------- Redemptions -------------------------------------------------------

-- RedemptionRequest


DROP TABLE IF EXISTS `redemptionrequest`;

CREATE TABLE `redemptionrequest` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `investmentapplicationid` BIGINT(12) NULL,
  `investmentbonddtlsid` BIGINT(12) NULL,
  `memberid` BIGINT(12) NULL,
  `bankdtlsid` BIGINT(12) NULL,
  `agreementreturned` VARCHAR(1) NULL,
  `appliedby` VARCHAR(45) NULL,
  `reference` VARCHAR(45) NULL,
  `applieddate` DATE NULL,
  `mobilenum` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));



  
-- RedemptionBatch  


DROP TABLE IF EXISTS `redemptionbatch`;

CREATE TABLE `redemptionbatch` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `batchdate` DATE NULL,
  `totalnumberofredemptions` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- RedemptionBatchdtls


DROP TABLE IF EXISTS `redemptionbatchdtls`;


CREATE TABLE `redemptionbatchdtls` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redemptionbatchid` BIGINT(12) NULL,
  `totalpayableamount` DECIMAL(16,8) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));


-- MemberFinalsettlement

DROP TABLE IF EXISTS `memberfinalsettlement`;

CREATE TABLE `memberfinalsettlement` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redemptionbatchdtlsid` BIGINT(12) NULL,
  `principleamount` DECIMAL(16,8) NULL,
  `interestamount` DECIMAL(16,8) NULL,
  `TDSamount` DECIMAL(16,8) NULL,
  `financialyear` VARCHAR(45) NULL,
  `paymentstartdate` DATE NULL,
  `paymentenddate` DATE NULL,
  `totalpaiddays` BIGINT(12) NULL,
  `totalpayableamount` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

  
  
-- InitiateBenificiaryRedemptions

DROP TABLE IF EXISTS `initiatebenificiaryredemption`;

CREATE TABLE `initiatebenificiaryredemptions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redemptionbatchid` BIGINT(12) NULL,
  `raisedby` BIGINT(12) NULL,
  `raiseddate` DATE NULL,
  `modeofpayment` VARCHAR(45) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));


-- RedemptionApproval

DROP TABLE IF EXISTS `redemptionapproval`;

CREATE TABLE `redemptionapproval` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `initiatebenificiaryredemptionid` BIGINT(12) NULL,
  `amount` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- RedemptionApprovalRemarks


DROP TABLE IF EXISTS `redemptionapprovalremarks`;

CREATE TABLE `redemptionapprovalremarks` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redemptionapprovalid` BIGINT(12) NULL,
  `approverid` BIGINT(12) NULL,
  `remarks` VARCHAR(125) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

  
-- RedemptionTransfer

DROP TABLE IF EXISTS `redemptiontransfer`;


CREATE TABLE `redemptiontransfer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redemptionbatchid` BIGINT(12) NULL,
  `initiatebenificiaryredemptionid` BIGINT(12) NULL,
  `chequenum` BIGINT(12) NULL,
  `bankdtlsid` BIGINT(12) NULL,
  `amount` DECIMAL(16,8) NULL,
  `transactionid` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

  
-- ----------------------------------------------------- OCR -----------------------------------------------------------------------


  
-- OcrRequest

DROP TABLE IF EXISTS `ocrrequest`;


CREATE TABLE `ocrrequest` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `surname` VARCHAR(45) NULL,
  `fullname` VARCHAR(45) NULL,
  `aadharnum` BIGINT(12) NULL,
  `receiptnum` BIGINT(12) NULL,
  `filepath` VARCHAR(45) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
-- Address master table

DROP TABLE IF EXISTS `addressstg`;

CREATE TABLE `addressstg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `Address1` VARCHAR(512) NULL,
  `Address2` VARCHAR(512) NULL,
  `countrymstid` BIGINT(10) NULL,
  `statemstid` BIGINT(10) NULL,
  `districtmstid` BIGINT(10) NULL,
  `citymstid` BIGINT(10) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
  
-- Employeestg

DROP TABLE IF EXISTS `employeestg`;

CREATE TABLE `employeestg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(45) NULL,
  `middlename` VARCHAR(45) NULL,
  `lastname` VARCHAR(45) NULL,
  `gender` VARCHAR(1) NULL,
  `dateofbirth` DATE NULL,
  `dateofjoining` DATE NULL,
  `emailid` VARCHAR(45) NULL,
  `mobilenum` BIGINT(12) NULL,
  `bloodgroup` VARCHAR(10) NULL,
  `emergencycontactnum` BIGINT(12) NULL,
  `aadharcardnum` VARCHAR(45) NULL,
  `pancard` VARCHAR(45) NULL,
  `photourl` VARCHAR(70) NULL,
  `paddressstgid` BIGINT(12) NULL,
  `caddressstgid` BIGINT(12) NULL,
  `bankdtlsid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

  
-- Employeeeducationaldtlsstg


DROP TABLE IF EXISTS `employeeeducationaldtlsstg`;


CREATE TABLE `employeeeducationaldtlsstg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `qualificationname` VARCHAR(45) NULL,
  `yearofpassing` BIGINT(12) NULL,
  `percentage` BIGINT(12) NULL,
  `employeestgid` BIGINT(12) NULL,
  `university` VARCHAR(45) NULL,
  `college` VARCHAR(45) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- EmployeeexperienceStg

DROP TABLE IF EXISTS `employeeexperiencestg`;

CREATE TABLE `employeeexperiencestg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `companyname` VARCHAR(105) NULL,
  `designation` VARCHAR(45) NULL,
  `from` DATE NULL,
  `to` DATE NULL,
  `lastsalarydrawn` BIGINT(20) NULL,
  `employeestgid` BIGINT(12) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

 
-- MemeberStg

DROP TABLE IF EXISTS `memberstg`;

CREATE TABLE `memberstg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `branchmstid` VARCHAR(45) NULL,
  `receiptid` VARCHAR(45) NULL,
  `receiptdate` DATE NULL,
  `typeofmembership` VARCHAR(45) NULL,
  `photo` BLOB NULL,
  `surname` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `gouthram` VARCHAR(45) NULL,
  `fathersname` VARCHAR(45) NULL,
  `husbandname` VARCHAR(45) NULL,
  `mothersname` VARCHAR(45) NULL,
  `maritalstatus` VARCHAR(45) NULL,
  `gender` VARCHAR(1) NULL,
  `Isheperformingsandhyavandhanam` VARCHAR(1) NULL,
  `isdoingpooja` VARCHAR(1) NULL,
  `reason` VARCHAR(512) NULL,
  `dateofbirth` DATE NULL,
  `age` BIGINT(12) NULL,
  `bloodgroup` VARCHAR(45) NULL,
  `educationalcategory` VARCHAR(45) NULL,
  `educationalqualification` VARCHAR(45) NULL,
  `occupation` VARCHAR(45) NULL,
  `professionaldetails` VARCHAR(45) NULL,
  `typeoforganization` VARCHAR(45) NULL,
  `nameoforganization` VARCHAR(45) NULL,
  `designation` VARCHAR(45) NULL,
  `officeaddress` VARCHAR(125) NULL,
  `styleofnametobedisplayedoncertificates` VARCHAR(45) NULL,
  `ParticularsofPostRetirementEmploymentifany` VARCHAR(125) NULL,
  `paddressstgid` BIGINT(12) NULL,
  `caddressstgid` BIGINT(12) NULL,
  `mobilenum` BIGINT(20) NULL,
  `alternatemobilenum` BIGINT(20) NULL,
  `emailid` VARCHAR(45) NULL,
  `website` VARCHAR(105) NULL,
  `voterid` VARCHAR(45) NULL,
  `pancardno` VARCHAR(45) NULL,
  `rationcardno` VARCHAR(45) NULL,
  `passportno` VARCHAR(45) NULL,
  `aadharcardno` VARCHAR(45) NULL,
  `istdaapplicable` VARCHAR(1) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));

-- FamilydtlsStg

DROP TABLE IF EXISTS `familydtlsstg`;

CREATE TABLE `familydtlsstg` (
  `id` INT NOT NULL  AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `relationship` VARCHAR(45) NULL,
  `dateofbirth` DATE NULL,
  `gender` VARCHAR(1) NULL,
  `qualification` VARCHAR(512) NULL,
  `mobilenum` BIGINT(20) NULL,
  `occupation` VARCHAR(512) NULL,
  `addressstgid` BIGINT(11) NULL,
  `employeestgid` BIGINT(11) NULL,
  `memberstgid` BIGINT(11) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updatedby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
 -- BankdtlsStg

DROP TABLE IF EXISTS `bankdtlsstg`;


CREATE TABLE `bankdtlsstg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `bankname` VARCHAR(45) NULL,
  `accountnum` VARCHAR(45) NULL,
  `branchname` VARCHAR(45) NULL,
  `ifsccode` VARCHAR(45) NULL,
  `accountholdername` VARCHAR(45) NULL,
  `isactive` VARCHAR(1) NULL,
  `efffrom` DATE NULL,
  `effto` DATE NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
-- MemberAdditionalParticular'sStg

DROP TABLE IF EXISTS `memberadditionalparticularsstg`;

CREATE TABLE `memberadditionalparticularsstg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `memberstgid` BIGINT(12) NULL,
  `village` VARCHAR(45) NULL,
  `mandal` VARCHAR(45) NULL,
  `statemstid` BIGINT(12) NULL,
  `districtmstid` BIGINT(12) NULL,
  `introducedtodfiby` VARCHAR(45) NULL,
  `ismarried` VARCHAR(1) NULL,
  `nameandaddressoffather-in-law` VARCHAR(512) NULL,
  `gothramoffather-in-law` VARCHAR(45) NULL,
  `nameofmother-in-law` VARCHAR(45) NULL,
  `nameandaddressoffather` VARCHAR(45) NULL,
  `gothramoffather` VARCHAR(45) NULL,
  `iscontactreference` VARCHAR(45) NULL,
  `name` VARCHAR(45) NULL,
  `mobilenum` BIGINT(12) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));
  
  
-- MemberElectrolInformationstg


DROP TABLE IF EXISTS `memberelectrolinfomationstg`;


CREATE TABLE `memberelectrolinfomationstg` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `memberstgid` BIGINT(12) NULL,
  `villagepanchayat` VARCHAR(45) NULL,
  `MPP` VARCHAR(45) NULL,
  `ZPP` VARCHAR(45) NULL,
  `wardmember` VARCHAR(45) NULL,
  `muncipality` VARCHAR(45) NULL,
  `corporation` VARCHAR(45) NULL,
  `MLAconstituency` VARCHAR(45) NULL,
  `MLCconstituency` VARCHAR(45) NULL,
  `MPconstituency` VARCHAR(45) NULL,
  `status` VARCHAR(1) NULL,
  `createdby` VARCHAR(45) NULL,
  `createddate` DATE NULL,
  `updateby` VARCHAR(45) NULL,
  `updateddate` DATE NULL,
  PRIMARY KEY (`id`));


