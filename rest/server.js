var express = require('express'),
    app = express(),
    path = require('path'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    cors = require('cors'),
    cluster = require('cluster'),
    logger = require('morgan'),
    favicon = require('serve-favicon'),
    passport = require('passport'),
    db = require('./config/db').db(),
    config = require('./config/config'),
    api = require('./routes/api');

var forceSsl = function (req, res, next) {
    if (req.headers['x-forwarded-proto'] !== 'https') {
        return res.redirect(['https://', req.get('Host'), req.url].join(''));
    }
    return next();
};

// if (process.env.NODE_ENV === 'production') {
//     app.enable('trust proxy');
//     app.use(forceSsl);
// };

// *** config middleware *** //
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//CORS-enabled
app.use(cors());

app.use(function (req, res, next) {
    if (req.url === '/favicon.ico') {
        res.writeHead(200, { 'Content-Type': 'image/x-icon' });
        res.end();
    } else {
        next();
    }
});

/**
* Route Settings
*/
app.use('/aqua/api', api);
app.use('/fpImages', express.static(path.join(__dirname + '/img')));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    err.path = req.originalUrl;
    res.render('error', {
        message: err
    });
});

module.exports = app;