var AquaValuesModel = require('../models/AquaValuesModel.js');

/**
 * AquaValuesController.js
 *
 * @description :: Server-side logic for managing AquaValuess.
 */
module.exports = {

    /**
     * AquaValuesController.list()
     */
    getAllDetails: function (gateway,callback) {
        let query=AquaValuesModel.find({});
            query.sort({_id:-1});
            query.limit(10);
            query.where('gatewayid').equals(gateway)
       query.exec(function(err,AquaValues){
            if (err) {
             console.log("error in AquaValues"+err)
            }
           callback({data:AquaValues})
        });
    },

    getValuesForDashboard: function (gateway,callback) {
        let query=AquaValuesModel.find({});
        query.sort({_id:-1});
        query.limit(1);
        query.where('gatewayid').equals(gateway)
        query.exec(function(err,response){
            if(err) throw err;
        
                console.log("final response is"+JSON.stringify(response));
                callback({data:response})
            
        })

    },

    /**
     * AquaValuesController.show()
     */
    show: function (req, res) {
        var id = req.params.id;
        AquaValuesModel.findOne({_id: id}, function (err, AquaValues) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting AquaValues.',
                    error: err
                });
            }
            if (!AquaValues) {
                return res.status(404).json({
                    message: 'No such AquaValues'
                });
            }
            return res.json(AquaValues);
        });
    },

    /**
     * AquaValuesController.create()
     */
    create: function (req, res) {
        var AquaValues = new AquaValuesModel({
			pH : req.body.pH,
			Ammonia : req.body.Ammonia,
			Nitrate : req.body.Nitrate,
			dissolvedoxygen : req.body.dissolvedoxygen

        });

        AquaValues.save(function (err, AquaValues) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when creating AquaValues',
                    error: err
                });
            }
            return res.status(201).json(AquaValues);
        });
    },

    /**
     * AquaValuesController.update()
     */
    update: function (req, res) {
        var id = req.params.id;
        AquaValuesModel.findOne({_id: id}, function (err, AquaValues) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when getting AquaValues',
                    error: err
                });
            }
            if (!AquaValues) {
                return res.status(404).json({
                    message: 'No such AquaValues'
                });
            }

            AquaValues.pH = req.body.pH ? req.body.pH : AquaValues.pH;
			AquaValues.Ammonia = req.body.Ammonia ? req.body.Ammonia : AquaValues.Ammonia;
			AquaValues.Nitrate = req.body.Nitrate ? req.body.Nitrate : AquaValues.Nitrate;
			AquaValues.dissolvedoxygen = req.body.dissolvedoxygen ? req.body.dissolvedoxygen : AquaValues.dissolvedoxygen;
			
            AquaValues.save(function (err, AquaValues) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error when updating AquaValues.',
                        error: err
                    });
                }

                return res.json(AquaValues);
            });
        });
    },

    /**
     * AquaValuesController.remove()
     */
    remove: function (req, res) {
        var id = req.params.id;
        AquaValuesModel.findByIdAndRemove(id, function (err, AquaValues) {
            if (err) {
                return res.status(500).json({
                    message: 'Error when deleting the AquaValues.',
                    error: err
                });
            }
            return res.status(204).json();
        });
    }
};
