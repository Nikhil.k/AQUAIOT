"use stric"

var Sequelize = require('sequelize');
var config = require('./config');
var mongoose=require('mongoose');

var mySQLdb = null, mongoDB = null;

module.exports.db = function () {
    if (mySQLdb === null && mongoDB===null) {
        mySQLdb = new Sequelize(config.serverConfig.database.dbName, config.serverConfig.database.user, config.serverConfig.database.password, {
            host: config.serverConfig.database.host,
            port: 3306,
            dialect: 'mysql',
            dialectOptions: {
                multipleStatements: true
            },
            pool: {
                max: 60,
                min: 0,
                idle: 7200
            }
        });
        mongoDB=mongoose.connect(config.serverConfig.mongodb.database+'://'+config.serverConfig.mongodb.host+':27017/'+config.serverConfig.mongodb.dbName,{useMongoClient:true},function(err){
            if(err){
              console.log("error in mongodb"+err)
            }
            else{
            console.log("connection established mongodb in "+config.serverConfig.mongodb.host,
            config.serverConfig.mongodb.dbName)
            }
        });
        
       //Checking MySQL connection status

        mySQLdb.authenticate().then(function (err) {
            console.log(err);
            if (err) {
                console.log('There is connection in ERROR');
            } else {
                console.log('Connection has been established successfully to DB: ' + config.serverConfig.database.dbName + ' on ' + config.serverConfig.database.host + ':3306 with user: ' + config.serverConfig.database.user);

            }
        });
        // mongoDB.authentication().then(function(err){
        //     if (err) throw err;
        //     else
        //     console.log('Connection has been established succefully to Mongodb'+config.serverConfig.mongodb.database)
        // })

    }
    return {"mysql": mySQLdb, "mongo": mongoDB};
};

