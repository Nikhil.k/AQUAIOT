var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    jwt = require('jsonwebtoken'),
    crypto = require('crypto'),
    config = require('../config/config'),
    JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt,
    userRolemodel = require('../models').userroledtls,
    usermodel = require('../sqlmodels').usermst;

var jwtOptions = {}
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader();
jwtOptions.secretOrKey = 'fuNDpiTcH123$';

passport.use(new LocalStrategy(
    { usernameField: 'mobilenum', passReqToCallback: true },
    function (req, mobilenum, password, done) {

        var user_info = req.body;
        //console.log('Password from parameter: ' + password);
        //console.log('Password from user_info: ' + user_info.password);

        usermodel.find({ where: { mobilenum: mobilenum } }).then(function (userObj) {
            if (userObj === null) {
                done(null, false, { message: 'Incorrect Mobile Number.' });
                return;
            }
            var hashPassword = crypto.createHash('md5').update(user_info.password).digest('hex');
            //console.log('hashPassword variable after md5 hash: ' + hashPassword);
            //console.log('Password from DB: ' + userObj.password);
            if (hashPassword != userObj.password) {
                done(null, false, { message: 'Incorrect Mobile Number.' });
                return;
            }
            done(null, userObj);
            return;
        })
    }
));

passport.use(new JwtStrategy(jwtOptions, function (jwt_payload, done) {
    console.log(jwt_payload)
    var mobilenum = JSON.parse(jwt_payload.mobilenum);
    usermodel.find({
        attrributes: ['rolemstid'],
        where: { mobilenum: mobilenum },
    }).then(function (result) {
        if (result === null) {
            return done(null, false, { message: 'Invalid token.' });
        }
        return done(null, result);
    }, function (err) {
        return done(null, false, { message: 'Invalid token.' });
    })
}));
module.exports.init = function () {
    return passport.initialize();
};

// use this to verify a JWT token on API routes
module.exports.tokenAuth = passport.authenticate('jwt', { session: false });
