"use strict"

var express = require('express'),
    router = express.Router(),
    multer = require('multer'),
    path = require('path'),
    fs = require("fs"),
    // commonService = require('../../services/commonService'),
    employeeService = require('../../services/storageService');
var profileImg = "";

var storage = multer.diskStorage({

    destination: path.join(__dirname, '../../img/profile'),
    filename: function (req, file, cb) {
        let datenow = Date.now();
        profileImg = file.originalname.replace(path.extname(file.originalname), "") + '-' + datenow + path.extname(file.originalname)
        cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' + datenow + path.extname(file.originalname))
    }
})

var upload = multer({ storage: storage });

/**
     * @author Nagaraju
     * updateProfile: Updates the user password
     * Input - Takes an UserID and data to be required for Update
     *  Output - If the userid is correct details are updated correctly, the record is returned, else an error or error response is returned
     *  URL : http://localhost:8080/fp/api/user/updateProfile                
*/
router.post('/updateProfile', function (req, res) {
    userService.updateProfile(req.body, function (response) {
        res.send(response);
    })
});

/**
    * @author Nagaraju
    * updatedPassword: Updates the user password
    * Input -   email , and  Password
    *  Output - is a callback function. If the details are updated correctly, the record is returned, else an error or error response is returned
    *  URL : http://localhost:8080/fp/api/user/updatedPassword                
*/
router.post('/updatedPassword', function (req, res) {
    userService.updatedPassword(req.body, function (response) {
        res.send(response);
    })
});


/**
 * @author Nagaraju
 * changePassword: change the existing password of the user based on userid  
 *                 and updated password gets updated into usermst table.
 * Input : email , Old Password and New Password
 * URL : http://localhost:8080/fp/api/user/changePassword
 */
router.post('/changePassword', function (req, res) {
    userService.changePassword(req.body, function (response) {
        res.send(response);
    })
});

/**
    * @author Nagaraju
    * getDomainType: It will displays all the domain names
    * URL : http://localhost:8080/fp/api/user/getDomainType
    */

router.get('/getDomainType', function (req, res) {
    userService.getDomainType(function (response) {
        res.send(response);
    })
});

router.get('/getProposals', function (req, res) {
    userService.getProposals(function (response) {
        res.send(response);
    })
});



router.post('/setPassword', function (req, res) {
    userService.setPassword(req.body, function (response) {
        res.send(response);
    })
});


// router.post('/profileImgUpload/:id', upload.single('file'), function (req, res) {
//     var data = { email: req.headers.email, image: '/supremeNetimg/profile/' + req.file.filename };
//     userService.profileImgUpload(data, function (response) {
//         res.send(response);
//     });
// });

router.post('/profileImgUpload/:id', upload.single('file'), function (req, res) {
    var data = { id: req.params.id, image: profileImg };
    userService.profileImgUpload(data, function (response) {
        res.send(response);
    });
});


/* For downloading files -- venkat */
router.get("/profileimage/:imagename", function (req, res) {
    console.log('profileimage...');
    var file = path.join(__dirname, '../../img/profile/') + req.params.imagename;
    if (fs.existsSync(file)) {
        res.setHeader('Content-disposition', 'inline');
        res.setHeader('Content-type', 'image/png');
        var filestream = fs.createReadStream(file);
        filestream.pipe(res);
    }
});




router.post('/updateEmail', function (req, res) {
    userService.updateEmail(req.body, function (response) {
        res.send(response);
    })
});

/** 
   * @author Nikhil
   * getUsers:Display's the details
   * of users which are approved by Admin
   */
router.get('/getUsers', function (req, res) {
    userService.getUserCountById(function (response) {
        res.send(response);
    })
});

router.post('/removeUser', function (req, res) {
    userService.removeUser(req.body, function (response) {
        res.send(response);
    });
});

router.post('/getTransactions', function (req, res) {
    userService.getTransactions(req.body, function (response) {
        res.send(response);
    });
});

/** 
     * @author Nikhil
     * generateCredentials:Generates UserId and 
     * Password of the particular user which are approved by
     * Admin by calling MYSQL procedure generateusercredentials
     */
router.post('/generateCredentials', function (req, res) {
    userService.generateCredentials(req.body, function (response) {
        res.send(response);
    })
});
/** 
   * @author Nikhil
   * getPendingRegistration:Display's the Pending
   * Registrations of Users who are waiting for Admin's Approval
   * by calling getPendingRegistration function which is present in
   * userService
   */

router.get('/getPendingRegistrations', function (req, res) {
    userService.getPendingRegistration(function (response) {
        res.send(response);
    })
});

/**
 *  @author Sarat Chandra
 * This route is used to insert a user and user-role association into the database 
 * URL: http://localhost:8080/fp/api/user/newUser
 */
router.post('/newUser', function (req, res) {
    userService.insertUser(req.body, function (response) {
        res.send(response);
    })
});

/**
 *  @author Sarat Chandra
 * This route is used to get list of projects created by business owner based on businessowernerid  
 * URL: http://localhost:8080/fp/api/user/getProjectsByUserID/<businessownerid>
 */

router.get('/getProjectsCreatedByUserID/:businessownerid', function (req, res) {
    userService.getProjectsCreatedByUserID(req.params.businessownerid, (response) => {
        res.send(response);
    });
});

/** 
     * @author : M M Ansari 
     * updateUserStatus() function - Updates user status to approved , Rejected , Inactive
     * Input - takes ' userId ' , ' status ' and ' updated by ' fields to updated the status to the DB for the given  userId.
     * Output - is a callback function that will be returned with updated data or with an error or error response
*/

router.post('/updateUserStatus', function (req, res) {
    // calling updatedUserStatus function to update the status of an user.
    userService.updateUserStatus(req.body, function (response) {
        res.send(response);
    })
});

// router.get('/getopenprojectsforbusinessowner/:businessownerid', function (req, res)  {
//     userService.getopenprojectsforbusinessowner(req.params.businessownerid, (response) => {
//         res.send(response);
//     });
// });

router.get('/getBussinessApprovedProjects/:businessuserid', function (req, res) {
    userService.getBussinessApprovedProjects(req.params.businessuserid, (response) => {
        res.send(response);
    });
});
/** 
   * @author Suresh
   * getReuiredUserDtls:This function is used to displays the required UserMaster Table details.
   * and rolemstid from userroledtls Table
   * OutPut: This function is used to display the Usermst required details
   * URL:http://localhost:8080/fp/api/user/getRequiredUserDtls
*/
router.post('/getRequiredUserDtls', function (req, res) {
    userService.getRequiredUserDtls(req.body, function (response) {
        res.send(response);
    })
});
/** 
   * @author Suresh
   * getRoleMstDtls:This function is used to displays the rolenm from  rolemst Table.
   * OutPut: This function is used to display the rolemst rolenm details.
   * URL:http://localhost:8080/fp/api/user/getRoleMstDtls
*/
router.get('/getRoleMstDtls', function (req, res) {
    userService.getRoleMstDtls(function (response) {
        res.send(response);
    })
});

/**
 * @author Lavanya
 * 
 *  URL:http://localhost:8080/fp/api/user/checkEmailExistOrNot/<input>
 */
router.get('/checkEmailExistOrNot/:email', function (req, res) {
    userService.checkEmailExistOrNot(req.params.email, function (response) {
        res.send(response);
        // console.log(response.emailId);
    })
    console.log(res.emailId);
});


/**
 * @author swapnika
 * getUserById:retreives a user data by passing id 
 * URL: http://localhost:8080/fp/api/user/getUserById    
 * input:{"id":number}
 */
router.post('/getUserById', (req, res) => {
    console.log("in router getuserbyid");
    userService.getUserById(req.body, (response) => {
        res.send(response);
    })
});

/**
 * @author swapnika
 * approveUser:calls generateCredentials and updateUserStatus
 * URL: http://localhost:8080/fp/api/user/approveUser    
 * input:{"id":number,"approvedBy":"name"}
 */
router.post('/approveUser', function (req, res) {
    let data = {
        "id": req.body.id,
        "userstatus": "approved",
        "remarks": "hjvgfkjfhjj",
        "approvedby": req.body.approvedBy
    };
    console.log("in router approve user " + req.body.id);
    userService.generateCredentials(data, function (response) {
        let datatosend = {
            "params": {
                "id": req.body.id
            },
            "actualData": {
                "updatedBy": req.body.approvedBy,
                "status": "Approved"
            }
        };
        console.log("in calling function");
        userService.updateUserStatus(datatosend, function (responsedata) {
            console.log("approved successfully" + JSON.stringify(response));
        }
        );
        let emailData =
            {
                subject: "FundPitch Credentials",
                toEmail: req.body.email,
                templateName: "approveUser",
                params: {
                    userId: response.newid,
                    password: response.newpwd,
                    email: req.body.email
                }
                // textContent:"you userID is "+response.newid+" password is "+response.newpwd,
            }
        commonService.sendMailNotification(emailData, function (responsedata) {
            console.log("mail sent==============");
        });
        res.send(response);

    });
});





/**
 * @author swapnika
 * rejectUser:calls updateUserStatus
 * URL: http://localhost:8080/fp/api/user/rejectUser    
 * input:{"id":number}
 */
router.post('/rejectUser', function (req, res) {
    console.log("in router reject user");
    let datatosend = {
        params: {
            id: req.body.id
        },
        actualData: {
            approvedBy: req.body.approvedBy,
            updatedBy: "sai",
            status: "Rejected"
        }
    };
    console.log("in reject router" + JSON.stringify(req.body));
    userService.updateUserStatus(datatosend, function (response) {
        res.send(response);
    }
    )
});

router.get('/getNewUsers', function (req, res) {
    userService.getNewUsers(function (response) {
        res.send(response);
    })
});

router.get('/getOpenProjectsforAdmin', function (req, res) {
    userService.getOpenProjectsforAdmin(function (response) {
        res.send(response);
    });
});
/**
  * @author Nikhil
  * resetPassword:The below route is  used to 
  * send's OTP to registered email address
  * and  mobile number
   */

// router.post('/resetPassword', function (req, res)  {
//     userService.resetPassword(req.body,function(response){
//         res.send(response);
//     });
// });
/**
 * @author Nikhil
 * updatePassword:This below route is used to update
 * the password after verifying the OTP successfully 
 */
router.post('/updatePassword', function (req, res) {

    userService.updatePassword(req.body, function (response) {

        res.send(response);
    });
});

/**
     * @author Nikhil
     * checkEmailAvailOrNot:This below route is used to check 
     * if email address is available or not 
     */
router.get('/checkEmailAvailOrNot/:email', function (req, res) {
    userService.checkEmailAvailOrNot(req.params.email, function (response) {
        res.send(response);
        // console.log(response.emailId);
    })
    console.log(res.emailId);
});

router.post('/AdminRegistration', function (req, res) {
    userService.adminRegistration(req.body, function (response) {
        res.send(response);
    })
});

router.post('/createEmployee', function (req, res) {

    employeeService.createEmployee(req.body, function (response) {
        res.send(response);
    })
});

router.get('/dropdowndesignation', (req, res) => {
    employeeService.dropdowndesignation(function (response) {
        res.send(response);
    })
});

router.get('/dropdowntoe', (req, res) => {
    employeeService.dropdowntoe(function (response) {
        res.send(response);
    })
});

router.get('/dropdownbu', (req, res) => {
    employeeService.dropdownbu(function (response) {
        res.send(response);
    })
});

router.get('/dropdownrole', (req, res) => {
    employeeService.dropdownrole(function (response) {
        res.send(response);
    })
});

router.get('/dropdownlocationname', (req, res) => {
    employeeService.dropdownlocationname(function (response) {
        res.send(response);
    })
});

router.get('/getIncomeTaxLables', (req, res) => {
    employeeService.getIncomeTaxLables(function (response) {
        res.send(response);
    })
});

router.post('/postItDeclsData', function (req, res) {
    console.log('inside saveitdecls');
    console.log(req.body.id);
    employeeService.postItDeclsData(req.body, function (response) {
        res.send(response);
    })
});

router.get('/reportingManager', (req, res) => {
    employeeService.reportingManager(function (response) {
        res.send(response);
    })
});

router.post('/resetPassword', function (req, res) {
    employeeService.resetPassword(req.body, function (response) {
        res.send(response);
    })
});


router.get('/getEmployeeDetails', function (req, res) {
    employeeService.getEmployeeDetails( function (response) {
        res.send(response);
    })
});
router.post('/insertlogin', function (req, res) {
    employeeService.insertlogin( req.body,function (response) {
        res.send(response);
    })
});
module.exports = router;