var express = require('express');
var router = express.Router();
var AquaValuesController = require('../../controllers/AquaValuesController');

/*
 * GET
 */
router.get('/getAllDetails/:gateway', function(req,res){
    AquaValuesController.getAllDetails(req.params.gateway,function(response){
        res.send(response)
    })
});

/*
 * GET
 */
router.get('/getValuesForDashboard/:gateway',function(req,res){
    AquaValuesController.getValuesForDashboard(req.params.gateway,function(response){
        res.send(response)
    })
});

/*
 * POST
 */
router.post('/', AquaValuesController.create);

/*
 * PUT
 */
router.put('/:id', AquaValuesController.update);

/*
 * DELETE
 */
router.delete('/:id', AquaValuesController.remove);

module.exports = router;
