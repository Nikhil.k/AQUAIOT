var express = require('express'),
    router = express.Router(),
    multer = require('multer'),
    path = require('path'),
    fs = require("fs"),
    // userService = require('../../services/userService'),
    pondService = require('../../services/pondService');


router.get('/getGatewayDetailsByUserid/:id', function (req, res) {
    pondService.getGatewayDetailsByUserid(req.params.id, function (response) {
        res.send(response);
    })
});

router.get('/getPendingPondDetails', function (req, res) {
    pondService.getPendingPondDetails(function (response) {
        res.send(response);
    });
});

router.post('/savePondDetails', function (req, res) {
    pondService.savePondDetails(req.body, function (response) {
        res.send(response);
    });
});




module.exports = router;