"use strict"

var express = require('express'),
    router = express.Router(),
    multer = require('multer'),
    path = require('path'),
    fs = require("fs"),
    // userService = require('../../services/userService'),
    userService = require('../../services/userService');
var profileImg = "";

var storage = multer.diskStorage({
    destination: path.join(__dirname, '../../img/profile'),
    filename: function (req, file, cb) {
        let datenow = Date.now();
        profileImg = file.originalname.replace(path.extname(file.originalname), "") + '-' + datenow + path.extname(file.originalname)
        cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' + datenow + path.extname(file.originalname))
    }
})


var upload = multer({ storage: storage });


router.post('/registerUserDetails', function (req, res) {
    console.log(">>>>>>>>><<<<<<<<", req.body);
    userService.registerUserDetails(req.body, function (response) {
        res.send(response);
    })
});

router.post('/uniqueEmail', function (req, res) {
    console.log(">>>>>>>>", req.body);
    userService.uniqueEmail(req.body, function (response) {
        res.send(response);
    })
});

router.post('/uniqueMobile', function (req, res) {
    userService.uniqueMobile(req.body, function (response) {
        res.send(response);
    })
});


router.post('/getUserDetails', function (req, res) {
    userService.getUserDetails(req.body, function (response) {
        res.send(response);
    });
});

router.post('/updateUserDetails', function (req, res) {
    console.log("---------===========", req.body);
    userService.updateUserDetails(req.body, function (response) {
        res.send(response);
    });
})

router.post('/dataExistOrNot', function (req, res) {
    userService.dataExistOrNot(req.body, function (response) {
        res.send(response);
        console.log(response);
    })
    console.log(res.gateway);
});


router.post('/updateData', function (req, res) {
    userService.updateData(req.body, function (response) {
        res.send(response);
        console.log(response);
    })
    console.log(res.gateway);
});

router.get('/getUserById/:id', function (req, res) {
    userService.getUserById(req.params.id, function (response) {
        res.send(response);
        console.log(response);
    })
});




router.get('/getCountryList', function (req, res) {
    userService.getCountry(function (response) {
        res.send(response);
    });
});



router.get('/getStatesByCountry/:id', function (req, res) {
    userService.getState(req.params.id, function (response) {
        res.send(response);
    });
});



router.get('/getDistrictsByState/:id', function (req, res) {
    userService.getDistrict(req.params.id, function (response) {
        res.send(response);
    });
});



router.get('/getCitiesByState/:id', function (req, res) {
    userService.getCity(req.params.id, function (response) {
        res.send(response);
    });
});


router.post('/generateCredentials', function (req, res) {
    userService.generateCredentials(req.body, function (response) {
        res.send(response);
    });
});


router.post('/sendOtp', function (req, res) {
    userService.sendotp(req.body, function (response) {
        res.send(response);
    });
});
router.post('/resetPassword', function (req, res) {
    userService.resetPassword(req.body, function (response) {
        res.send(response);
    });
});

router.post('/updatePassword', function (req, res) {
    userService.updatePassword(req.body, function (response) {
        res.send(response);
    });
});
module.exports = router;