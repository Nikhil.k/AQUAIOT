"use strict"

var express = require('express'),
    router = express.Router(),
    passport = require('passport'),
    verifyToken = require('../../config/auth').tokenAuth,
    jwt = require('jsonwebtoken'),
    // register_api = require('./register'),
    // leave_api = require('./leave'),
    // investor_api = require('./investor'),
    // professional_api = require('./professional'),
    config = require('../../config/config'),
    user_api = require('./user'),
    // project_api = require('./project'),
    pondServices_api=require('./pond'),
    storage_api = require('./storage'),
    // candidate_api=require('./candidate'),
    aqua_api = require('./AquaValuesRoutes');


router.post('/token', function (req, res, next) {
    console.log(req.body);
    passport.authenticate('local', function (err, userObj, info) {
        if (err) {
            return next(err);
        }
        if (!userObj) {
            return res.status(401).json(info);
        }
        var token = jwt.sign(
            { mobilenum: JSON.stringify(userObj.mobilenum) },
            config.auth.tokenSecret,
            { expiresIn: '1 days' }
        );
        return res.json({ token: token });
    })(req, res, next);
});

router.get('/me', passport.authenticate('jwt', { session: false }), function (req, res) {
    res.send({ user_info: req.user });
});

// RESTful resources
// router.use('/register', register_api);
router.use('/user', user_api);
// router.use('/leave', leave_api);
// router.use('/investors', investor_api);
// router.use('/professionals', professional_api);

// Using the Route /cnr (clarification and response) for all clarifications and responses
// router.use('/cnr', project_api);
router.use('/pond', pondServices_api);
router.use('/AquaValuesRoutes', aqua_api);
router.use('/storage', storage_api);
// router.use('/candidate', candidate_api);
module.exports = router;