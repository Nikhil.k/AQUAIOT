DELIMITER $$
DROP PROCEDURE IF EXISTS generateusercredentials$$
CREATE PROCEDURE generateusercredentials
(
	IN ip_usermasterid   mediumint(10), -- input usermaster id
	IN ip_userapprovalstatus   varchar(45), -- checks whether employee is created or rejected
	IN ip_bgcremarks   varchar(512), -- remarks if userid and password is not generated
	IN ip_approvedby   varchar(512), -- user who ever approves  
	OUT op_newnewuserid varchar(255), -- newly generated userid
	OUT op_newpassword varchar(255),-- newly generated password
	OUT op_returncode varchar(10)-- code to refer whether the userid and password is generated or NOT
  
)
BEGIN
DECLARE v_username varchar(45);
DECLARE v_lastname varchar(45);
DECLARE v_user varchar(45);
DECLARE v_randId INT ;
-- DECLARE v_count INT ;
DECLARE EXIT HANDLER FOR 1456 SELECT '1456' into op_returncode;
  
SET max_sp_recursion_depth=18; 

SET op_returncode = '001'; -- code-001 if the user already exists

SELECT userid into v_user from usermst where id=ip_usermasterid;
 
IF ip_userapprovalstatus = 'Rejected' THEN
	SET op_returncode = '002'; -- code-002 if the user doesnot exist
	SET op_newnewuserid = '';
		UPDATE usermst SET 
			userstatus = ip_userapprovalstatus,
			remarks = ip_bgcremarks,
			approvedby = ip_approvedby,
			approveddate = NOW()
		WHERE id = ip_usermasterid;

ELSE IF v_user IS NULL THEN
	
	SELECT concat(substring(firstname,1,3),substring(lastname,1,3))  FROM usermst WHERE id = ip_usermasterid INTO v_username;
 
	SELECT floor(RAND() * 10000) from dual into v_randId;
 
	SET v_username = concat(v_username,v_randId);
 
	-- select count(*) from usermst where userid = v_username INTO v_count;
	 SET op_newnewuserid = v_username;
		 
		 SELECT CONV(FLOOR(RAND() * 9999999), 20, 36) INTO op_newpassword; 
			SET op_returncode = '000'; -- code-000 userid and password generated successfully
			UPDATE usermst SET 
				userid = op_newnewuserid, 
				password = MD5(op_newpassword),
				userstatus = ip_userapprovalstatus,
				remarks = ip_bgcremarks,
				approvedby = ip_approvedby,
				approveddate = NOW()
			WHERE id = ip_usermasterid; 

  -- IF v_count > 0 THEN
		
	ELSE
	
	   SET op_returncode='001';
	
		END IF;

END IF;

 
END
DELIMITER ;