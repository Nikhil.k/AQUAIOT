let multer = require('multer');



function storageService() {

        this.pondImageUpload=function(callback){

            let storage =   multer.diskStorage({
                destination: function (req, file, callback) {
                  callback(null, '../img');
                },
                filename: function (req, file, callback) {
                  callback(null, file.fieldname + '-' + Date.now());
                }
              });
              let upload = multer({ storage : storage}).single('userPhoto');
        }

}

storageService = new storageService();

module.exports = storageService;