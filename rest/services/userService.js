"use strict"

// let userModel = require('../models/user'),
// subscriptionModel = require('../models/subscriptions'), 
let path = require('path'),
    fs = require('fs'),
    sequelize = require('../config/db').db().mysql,
    md5 = require('md5'),
    userMstModel = require('../sqlmodels').usermst,
    roleMstModel = require('../models').rolemst,
    pondGateWayModel = require('../sqlmodels').pondgateway,
    userPondDtlsModel = require('../sqlmodels').userponddtls,
    countryModel = require('../sqlmodels').countrymst,
    stateModel = require('../sqlmodels').statemst,
    districtModel = require('../sqlmodels').districtmst,
    cityModel = require('../sqlmodels').citymst,
    nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport'),
    Client = require('node-rest-client').Client,
    templates = require('../config/emailTemplates'),
    config = require('../config/config');


function authService() {


    /** 
    * @author Nikhil
    * generateCredentials:This function generates UserId and 
    * Password of the particular user which are approved by
    * Admin by calling MYSQL procedure generateusercredentials
    */


    this.generateCredentials = function (data, callback) {

        let query = " SET @p1 = :id; SET @p2 = :userstatus; " +
            " SET @p3 = :remarks; SET @p4 = :approvedby; " +
            " SET @p5 = null; SET @p6 = null; SET @p7 = null;" +
            "call generateusercredentials(@p1,@p2,@p3,@p4,@p5,@p6,@p7); SELECT @p5 as newid,@p6 as newpwd, @p7 as rtcode;";
        sequelize.query(query,
            {
                replacements: {
                    id: data.id,
                    userstatus: data.userstatus,
                    remarks: data.remarks,
                    approvedby: data.approvedby
                }
            })
            .then(function (rows) {
                // console.log(rows);

                let retjson = rows[0][8][0];
                if (rows[0][8][0].newid == null) {
                    retjson.newid = null;
                    retjson.newpwd = null;
                    retjson.rtcode = "001";
                }
                callback(rows[0][8][0]);
            })
            .error(function (err) {
                callback(err);
            });
    };


    this.getUserDetails = function (data, callback) {
        if (data.status != null && data.status != undefined && data.status != "") {

            userMstModel.findAll({
                attributes: ['id', 'firstname', 'lastname', 'mobilenum', 'email', 'status'],
                where: {
                    status: data.status
                },
                order: [
                    ['updatedAt', 'DESC']
                ]
            }).then(function (response) {
                console.log("RESPONSE" + response);
                callback({ data: response })

            }, function (err) {
                console.log("error in response" + err)
            })
        }
    }

    this.registerUserDetails = function (data, callback) {
        data.status = "WaitingForCall"
        data.createddate = new Date();
        userMstModel.build(data).save().then((response) => {
            callback({ data: response })
        }, function (err) {
            console.log("error in data" + err)
        })
    }
    this.uniqueEmail = function (data, callback) {
        let response1 = false;
        userMstModel.find({
            where: { email: data.email }
        })
            .then(function (response) {
                if (response.email === data.email) {
                    response1 = true;
                }
                callback({ status: 0, data: response1 });
            })
            .catch(function (err) {
                callback({ status: 1, data: "false" });
            })
    }

    this.uniqueMobile = function (data, callback) {
        console.log("----", data)
        let response1 = false;
        userMstModel.find({
            where: { mobilenum: data.mobilenum }
        })
            .then(function (response) {
                if (response.mobilenum == data.mobilenum) {
                    console.log
                    response1 = true;
                }
                callback({ status: 0, data: response1 });
            })
            .catch(function (err) {
                callback({ status: 1, data: "false" });
            })
    }
    this.updateUserDetails = function (data, callback) {
        data.data.updatedAt = new Date();
        data.data.id = data.id;
        data.data.status = 'WaitingForPondDetails';
        // data.updatedAt.setTime(data.updatedAt.getTime() + (5 * 60 * 60 * 1000 + 30 * 60 * 1000));
        userMstModel.update(data.data, { where: { id: data.id } }).then((response) => {
            callback({ data: response, status: "User Details Updated..." })
        }, function (err) {
            console.log(err)
        })
    }

    this.dataExistOrNot = function (data, callback) {
        var isExist = true;
        if (data.aadharnumber != null && data.aadharnumber != undefined) {
            userMstModel.find({
                attributes: ['id'],
                where: {
                    aadharnumber: data.aadharnumber
                }
            })
                .then(function (response) {

                    if (response == null) {
                        isExist = false;
                    }
                    callback({ status: 0, isExist: isExist })
                }, function (err) {
                    callback(err);
                });
        }
        else if (data.pannumber != null && data.pannumber != undefined) {
            userMstModel.find({
                attributes: ['id'],
                where: {
                    pannumber: data.pannumber
                }
            })
                .then(function (response) {

                    if (response == null) {
                        isExist = false;
                    }
                    callback({ status: 0, isExist: isExist })
                }, function (err) {
                    callback(err);
                });
        }

        else if (data.gateway != null && data.gateway != undefined) {
            console.log("gateway" + data.gateway)
            pondGateWayModel.find({
                attributes: ['id'],
                where: {
                    gateway: data.gateway
                }
            })
                .then(function (response) {

                    if (response == null) {
                        isExist = false;
                    }
                    callback({ status: 0, isExist: isExist })
                }, function (err) {
                    callback(err);
                });
        }
        // else {
        //     callback({ isExist: isExist });
        // }
    }
    this.getCountry = function (callback) {
        let query = {
            attributes: ['id', 'countryname']
        };
        countryModel.findAll(query)
            .then(function (response) {
                // success callback
                callback({ status: 0, data: response });
            })
            .error(function (err) {
                // error callback
                callback({ status: 1, data: err });
            });
    };


    this.getState = function (id, callback) {
        var query = {
            attributes: ['statename', 'id'],
            where: {
                countrymstid: id
            }
        };
        stateModel.findAll(query)
            .then(function (response) {
                callback({ status: 0, data: response });
            })
            .error(function (err) {
                // error callback
                callback({ status: 1, data: err });
            });
    };



    this.getDistrict = function (id, callback) {
        var query = {
            attributes: ['districtame', 'id'],
            where: {
                statemstid: id
            }
        };
        districtModel.findAll(query)
            .then(function (response) {
                // success callback
                callback({ status: 0, data: response });
            })
            .error(function (err) {
                // error callback
                callback({ status: 1, data: err });
            });
    };


    this.getCity = function (id, callback) {
        var query = {
            attributes: ['cityname', 'id'],
            where: {
                statemstid: id
            },
            order: [
                ['cityname', 'ASC']
            ]
        };
        cityModel.findAll(query)
            .then(function (response) {
                // success callback
                callback({ status: 0, data: response });
            })
            .error(function (err) {
                // error callback
                callback({ status: 1, data: err });
            });
    };

    this.getUserById = function (userID, callback) {
        console.log(userID);
        userMstModel.findAll({
            // attributes: ['id', 'firstname', 'lastname', 'mobilenum', 'email', 'status'],
            where: {
                id: userID
            }
        }).then(function (response) {
            console.log("RESPONSE" + response);
            callback({ data: response })

        }, function (err) {
            console.log("error in response" + err)
        })
    }

    this.sendMail = function (data, callback) {

        nodemailer.createTestAccount(function (err, account) {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                service: "Gmail",
                auth: {
                    user: config.auth.gmail.user,
                    pass: config.auth.gmail.password
                }
            });
            // setup email data with unicode symbols
            let mailOptions = {
                from: "Supreme Netsoft Pvt Ltd <do-not-reply@Supremenetsoft.com>", // sender address
                to: data.toEmail, // list of receivers
                subject: data.subject, // Subject line
                // text: "hello"// plain text body
            };
            // send mail with defined transport object

            if (data.templateName !== undefined) {
                console.log(data.params);
                if (data.params === undefined) {
                    data.params = {};
                }
                mailOptions.html = templates.getTemplate(data.templateName, data.params);
            }
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    return console.log(error);
                }

                console.log('Message sent: %s', info.messageId);
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            });

        });
        callback({ status: 0 });
    }

    this.sendotp = function (data, callback) {
        let client = new Client();
        //   if(config.auth.){
        //Generate random numbers between 1000 and 9999
        if (data.templateName == 'AquaTemplate') {
            let minimum = 100000, maximum = 999999;
            let randomnumber = Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;

            let url = config.auth.otpServer.protocol + '://' + config.auth.otpServer.host + "/" + config.auth.otpServer.path + "/" + data.mobileNumber + "/" + randomnumber + "/AquaTemplate ";

            console.log("sendOtp service   " + url)

            client.get(url, function (data, response) {
                // parsed response body as js object
                // console.log(data);
                // raw response 
                callback({
                    data: data,
                    otpNumber: randomnumber
                });
            });
        }
        if (data.templateName == 'CredentialsTemplate') {

            let url = config.auth.otpServer.protocol + '://' + config.auth.otpServer.host + "/" + config.auth.otpServer.transactionalPath + "/";
            var args = {
                data: { 'From': config.auth.otpServer.credentialsSenderId, 'To': data.mobileNumber, 'TemplateName': 'CredentialsTemplate', 'VAR1': data.username, 'VAR2': data.userid, 'VAR3': data.password },
                headers: { "Content-Type": "application/json" }
            };
            client.post(url, args, function (data) {
                callback({
                    data: data
                })
            })
        }

        if (data.templateName == 'AquaValuesTemplate') {
          
            let url = config.auth.otpServer.protocol + '://' + config.auth.otpServer.host + "/" + config.auth.otpServer.transactionalPath + "/";
            var args = {
                data: { 'From': config.auth.otpServer.AquaValues, 'To': data.mobileNumber, 'TemplateName': 'AquaValuesTemplate ', 'VAR1': data.username, 'VAR2': data.currentLevel, 'VAR3': data.actualLevel },
                headers: { "Content-Type": "application/json" }
            };
            client.post(url, args, function (data) {
                callback({
                    data: data
                })
            })
        }

    }


    this.resetPassword = function (data, callback) {
        userMstModel.findAll({
            attributes: ['mobilenum', 'email'],
            where: {
                mobilenum: data.mobile
            }
        }).then((response) => {
            console.log("At line 377----" + JSON.stringify(response));
            console.log("At line 378----" + response[0].dataValues.mobilenum);
      this.sendotp({ templateName: 'AquaTemplate', mobileNumber: response[0].dataValues.mobilenum }, function (result) {
          console.log('otpNumber-------'+JSON.stringify(result.otpNumber));
          callback({data:response,data1:result});
            }, function (err) {
                console.log('error in sending Otp')
            })
        }, function (err) {
            console.log('error in getting mobilenumber')
        })
    }


    this.updatePassword = function (data, callback) {
        userMstModel.update({
            password: md5(data.newPassword)
        },
            {
                where: { mobilenum: data.mobilenum }
            }).then((response) => {

          callback({data:response})
            })

    }

}
authService = new authService();

module.exports = authService;