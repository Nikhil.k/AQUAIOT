
let userMstModel = require('../sqlmodels').usermst,
    roleMstModel = require('../models').rolemst,
    sequelize = require('../config/db').db().mysql,
    pondGateWayModel = require('../sqlmodels').pondgateway,
    userPondDtlsModel = require('../sqlmodels').userponddtls,
    countryModel = require('../sqlmodels').countrymst,
    stateModel = require('../sqlmodels').statemst,
    districtModel = require('../sqlmodels').districtmst,
    cityModel = require('../sqlmodels').citymst,
    userService = require('../services/userService');

function pondService() {


    this.getGatewayDetailsByUserid = function (id, callback) {
        userPondDtlsModel.belongsTo(userMstModel);

        userPondDtlsModel.findAll({
            attributes: ['ponddtls'],
            include: [{
                model: userMstModel,
                attributes: ['id'],
                where: {
                    id: id
                }
            }],

        })
            .then(function (response) {
                callback({ status: 0, data: response })
            }, function (err) {
                callback({ status: 1, data: err })
            })

    }

    this.savePondDetails = function (data, callback) {
        var isExist = true;
        data.data.createdAt = new Date();
        data.data.usermstid = data.usermstid;
        data.data.status = 'A';
        // console.log("data is..", data.data.ponddtls);
        userPondDtlsModel.build(data.data).save()
            .then(function (response) {
                // console.log('At line 45----------'+JSON.stringify(response))
                // console.log("At line 46----------"+response.dataValues.firstname)
                userMstModel.findAll({
                    attributes: ['firstname', 'email', 'mobilenum'],
                    where: {
                        id: data.usermstid
                    }
                }).then((mobileData) => {
                    console.log("after mobileData response" + JSON.stringify(mobileData));
                    console.log("At line 55-------------" + mobileData[0].dataValues.firstname)
                    let credentials = {
                        id: data.usermstid,
                        userstatus: 'Approved',
                        remarks: 'UserId and Password generated successfully',
                        approvedby: data.id
                    }
                    userService.generateCredentials(credentials, function (userCredentials) {
                        console.log("generate userCredentials function");
                        let emailData = {
                            subject: "SupremeNetSoft Credentials",
                            toEmail: mobileData[0].email,
                            templateName: "approveUser",
                            params: {
                                userId: mobileData[0].mobilenum,
                                password: userCredentials.newpwd,
                                // email: response.dataValues.emailid
                            }
                        }
                        console.log("emailData----------" + JSON.stringify(emailData))
                        userService.sendMail(emailData, function (emailResponse) {
                            console.log('SendMAil service-------' + JSON.stringify(emailResponse))
                        });
                        let otpData = {
                            templateName: 'CredentialsTemplate',
                            username: mobileData[0].dataValues.firstname,
                            mobileNumber: mobileData[0].mobilenum,
                            userid: mobileData[0].mobilenum,
                            password: userCredentials.newpwd
                        }
                        console.log('otpData' + JSON.stringify(otpData))
                        // console.log("response of OTP"+JSON.stringify(otpData))
                        userService.sendotp(otpData, function (otpResponse) {
                            callback({ data: response, data1: mobileData, data2: userCredentials, data4: otpResponse })
                        })
                    }, function (err) {
                        console.log('error in userCredentials data')
                    });

                }, function (err) {
                    console.log('error in response mobileData')
                });
            }, function (err) {
                console.log('error in Inserting into userPonddtls Table')
            });

    }


    this.getPendingPondDetails = function (callback) {
        userMstModel.findAll({
            where: {
                status: 'WaitingForPondDetails'
            },
            order: [['createdAt', 'desc']]

        }).then((response) => {
            callback({ data: response })
        }, function (err) {
            console.log("error in route")
        })
    }

}
pondService = new pondService();

module.exports = pondService;