/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('paymentdtls', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    usermstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'usermst',
        key: 'id'
      }
    },
    planmstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'planmst',
        key: 'id'
      }
    },
    planstartdate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    planenddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    createdby: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    updatedby: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    createddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'paymentdtls'
  });
};
