/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('countrymst', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    countryname: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    currencycd: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    currencyname: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    currencysymbol: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    createdby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    createddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    modifiedby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    modifieddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'countrymst'
  });
};
