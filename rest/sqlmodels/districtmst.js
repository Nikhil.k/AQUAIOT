/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('districtmst', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    districtame: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    statemstid: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'statemst',
        key: 'id'
      }
    },
    status: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    createdby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    createddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    modifiedby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    modifieddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'districtmst'
  });
};
