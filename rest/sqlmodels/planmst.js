/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('planmst', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    plandesc: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    totalplanamount: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    createddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'planmst'
  });
};
