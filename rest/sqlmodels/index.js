var sequelize = require('../config/db').db().mysql;

// load models
var models = [
  // Changed into lines for easy merging by Venkat.. added..fundpitchfiledtls
  'countrymst','statemst','citymst','districtmst','planmst','usermst','paymentdtls','userponddtls'
];

models.forEach(function (model) {
//   console.log(models)
  module.exports[model] = sequelize.import(__dirname + '/' + model)
});

// export connection
module.exports.sequelize = sequelize;
