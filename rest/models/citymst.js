/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('citymst', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    cityname: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    districtmstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'districtmst',
        key: 'id'
      }
    },
    statemstid: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    ismetro: {
      type: DataTypes.STRING(1),
      allowNull: true,
      defaultValue: 'N'
    },
    status: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    createdby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    createddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    modifiedby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    modifieddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'citymst'
  });
};
