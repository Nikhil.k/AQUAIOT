/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('statemst', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    statename: {
      type: DataTypes.STRING(256),
      allowNull: false
    },
    countrymstid: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'countrymst',
        key: 'id'
      }
    },
    status: {
      type: DataTypes.STRING(1),
      allowNull: true
    },
    createdby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    createddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    modifiedby: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    modifieddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'statemst'
  });
};
