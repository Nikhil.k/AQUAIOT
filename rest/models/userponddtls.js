/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('userponddtls', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    usermstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'usermst',
        key: 'id'
      }
    },
    ponddtls: {
      type: DataTypes.JSON,
      allowNull: true
    },
    status: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    createdby: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    updatedby: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    updatedate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    }
  }, {
    tableName: 'userponddtls'
  });
};
