var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var AquaValuesSchema = new Schema({
	'pH' : Number,
	'Ammonia' : Number,
	'Nitrate' : Number,
	'dissolvedoxygen' : Number
},{collection:'AquaValues'});

module.exports = mongoose.model('AquaValues', AquaValuesSchema);
