/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('usermst', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    firstname: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    lastname: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    gender: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    rolemstid: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    userid: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    mobilenum: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    email: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    countrymstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'countrymst',
        key: 'id'
      }
    },
    statemstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'statemst',
        key: 'id'
      }
    },
    districtmstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'districtmst',
        key: 'id'
      }
    },
    citymstid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'citymst',
        key: 'id'
      }
    },
    status: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    createddate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    updatedate: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    aadharnumber: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    pannumber: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    address1: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    address2: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    address3: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    zipcode: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    bankname: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    accountnumber: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    accountholdername: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    ifsccode: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    location: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'usermst'
  });
};
