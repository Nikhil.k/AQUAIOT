	module.exports.STATUS_CREATED = 'CREATED';
	module.exports.STATUS_CONFIRM = 'CONFIRM';
	module.exports.STATUS_REJECTED = 'REJECTED';
	module.exports.STATUS_PROCESSED = 'PROCESSED';



	/**
	 * for employee ctc and ctcdtls
	 *  
	 */
	module.exports.STATUS_ACTIVE = 'A';   // A - ACTIVE
	module.exports.STATUS_INACTIVE = 'X'; // X-INACTIVE
	module.exports.BASIC = 1;
	module.exports.HRA = 2;
	module.exports.SPLALL = 3;
	module.exports.MED = 4;
	module.exports.CONV = 5;
	module.exports.KIDEDU = 6;
	module.exports.ARR = 7;
	module.exports.PFO = 12;
	module.exports.MDI = 14;