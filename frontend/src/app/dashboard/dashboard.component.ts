import { Component, OnInit } from '@angular/core';
import { ChartService } from '../../app/Services/chart.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { AnonymousSubscription } from 'rxjs/Subscription';
import * as Plotly from 'plotly.js/lib/core';
import { ScatterData, Layout, PlotlyHTMLElement, newPlot } from 'plotly.js/lib/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ChartService]
})
export class DashboardComponent implements OnInit {
  gatewayid: any;
  pHData = [];
  AmmoniaData = [];
  NitrateData = [];
  dissolvedData = [];
  dashboardData: any;
  timeStamps = [];
  id: any;
  timer: AnonymousSubscription;

  graphDiv = '#dashboard';
  constructor(private dashboard: ChartService, private route: ActivatedRoute, ) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.id = params['gateway'];
      localStorage.setItem('hardware', this.id);
      console.log(localStorage.getItem('hardware'))
      this.gatewayid = localStorage.getItem('hardware')
    })
    this.refreshData();
 
  }

  subscribeToData(): void { 
    this.timer = Observable.timer(5000).first().subscribe(() =>
      this.refreshData()
    )
  }
  refreshData(): void { 
    this.dashboard.getChartValues(this.gatewayid).subscribe((response) => {
      this.dashboardData = response.data;
      for (let i = 0; i <= this.dashboardData.length; i++) {
        this.pHData.push(this.dashboardData[i].PH);
        this.AmmoniaData.push(this.dashboardData[i].Ammonia);
        this.NitrateData.push(this.dashboardData[i].Nitrate);
        this.dissolvedData.push(this.dashboardData[i].dissolvedoxygen)
        let date = this.dashboard.getDate(this.dashboardData[i].timestamps)
        this.timeStamps.push(date)
      }      
    })
    this.subscribeToData();

  }

  ngAfterViewInit() {
    const pH = {
      x: this.timeStamps,
      y: this.pHData,
      name: 'pH',
      type: 'scatter'
    } as ScatterData;
    const Ammonia = {
      x: this.timeStamps,
      y: this.AmmoniaData,
      name: 'Ammonia',
      type: 'scatter'
    } as ScatterData;
    const Nitrate = {
      x: this.timeStamps,
      y: this.NitrateData,
      name: 'Nitrate',
      type: 'scatter'
    } as ScatterData;
    const DissolvedOxygen = {
      x: this.timeStamps,
      y: this.dissolvedData,
      name: 'DissolvedOxygen',
      type: 'scatter'
    } as ScatterData;
    const data = [pH, Ammonia, Nitrate, DissolvedOxygen];
    const layout = {
      title: 'All Parameters',
      xaxis: {
        title: '----Time----',
        showgrid: false,
        zeroline: false
      },
      yaxis: {
        title: 'Values',
        showline: false
      },

    };
    //displayModeBar: false
    // staticPlot: true
    // showLink: True
    Plotly.newPlot(this.graphDiv, data, layout, { displayModeBar: false });

  }
}
