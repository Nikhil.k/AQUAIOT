import { UserService } from './../Services/user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthGuard } from '../gaurd/Authgaurd';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-technician',
  templateUrl: './technician.component.html',
  styleUrls: ['./technician.component.css'],
  providers: [UserService]
})
export class TechnicianComponent implements OnInit {
  public technicianForm: FormGroup;
  status: any;
  status1: any;
  userid: any;
  constructor(private _fb: FormBuilder, private userService: UserService, private activateRoute: ActivatedRoute, private authguard: AuthGuard) { }
  messageSuccess: any;
  ngOnInit() {
    this.technicianForm = this._fb.group({

      ponddtls: this._fb.array([
        this.initPond(),
      ])
    });

    this.activateRoute.queryParams.subscribe(params => {
      this.userid = params['userId']
    })
  }

  initPond() {
    return this._fb.group({
      'hardwareid': ['', [Validators.required]],
      'pondname': ['', [Validators.required]],
      'pondlocation': ['', [Validators.required]],
      "pondimage":['', [Validators.required]]
    });
  }

  addPond() {
    const control = <FormArray>this.technicianForm.controls['ponddtls'];
    control.push(this.initPond());
  }

  removePond(i: number) {
    const control = <FormArray>this.technicianForm.controls['ponddtls'];
    control.removeAt(i);
  }

  savePondDetails(data) {
    let pondData = {
      usermstid: this.userid,
      id: this.authguard.userid,
      data: data
    }
    console.log("savePonddtls"+JSON.stringify(pondData))
    this.userService.savePondDetails(pondData).subscribe((response) => {
      this.status = response.isExist;
      this.messageSuccess = this.status;
      console.log("savePonddtls in Technician Component "+JSON.stringify(response))

      setTimeout(() => {    //<<<---    using ()=> syntax
        this.status = false;
      }, 3000);
    });
  }

}
