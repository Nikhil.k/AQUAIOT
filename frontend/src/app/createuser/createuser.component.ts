import { UserService } from './../Services/user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
// import { FormBuilder } from '@angular/forms/src/form_builder';

declare var jQuery: any;
@Component({
  selector: 'app-createuser',
  templateUrl: './createuser.component.html',
  styleUrls: ['./createuser.component.css'],
  providers: [UserService]
})
export class CreateuserComponent implements OnInit {
  citylist: Array<string> = ['--select--', 'Inida', 'USA'];
  gender: any = [{ id: '', gender: '--select--' }, { id: 'M', gender: 'Male' }, { id: 'F', gender: 'Female' }, { id: 'O', gender: 'Others' }];
  role: any = [{ id: '', role: '--select--' }, { id: 1, role: 'Admin' }, { id: 2, role: 'user' }, { id: 3, role: 'Technician' }]; userForm: FormGroup;
  project: Array<any>;
  response: any;
  gateway: any;
  accountnumber: any;
  pannumber: any;
  aadharnumber: any;
  aadhar: any;
  pan: any;
  userDetails: {};
  hwid: any;
  updatedata: any;
  public countries: any;
  states: any;
  districts: any;
  cities: any;
  country: any;
  district: any;
  city: any;
  state: any;
  userId: any;
  constructor(private userService: UserService, private _fb: FormBuilder, private activatedRoute: ActivatedRoute) { }
  x: any;
  ngOnInit() {

    this.activatedRoute.queryParams.subscribe(params => {
      this.userId = params['usermstid'];
      console.log("create user component" + this.userId);
      this.userService.getUserById(this.userId).subscribe(data => {
        this.userDetails = data.data;
        console.log("userDetails to edit the masterData" + JSON.stringify(this.userDetails));
      });
    })


    this.userForm = new FormGroup({
      'firstname': new FormControl('', [Validators.required]),
      'lastname': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9!#$%&{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$')]),
      'pannumber': new FormControl('', [Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}'), Validators.required]),
      'aadharnumber': new FormControl('', [Validators.required, Validators.pattern('[0-9]+')]),
      'address1': new FormControl('', [Validators.required]),
      'address2': new FormControl(''),
      'address3': new FormControl(''),
      'country': new FormControl('', [Validators.required]),
      'state': new FormControl('', [Validators.required]),
      'district': new FormControl('', [Validators.required]),
      'city': new FormControl('', [Validators.required]),
      'pincode': new FormControl('', [Validators.required]),
      'zipcode': new FormControl('', [Validators.required]),
      'bankname': new FormControl('', [Validators.required]),
      'accountnumber': new FormControl('', [Validators.required]),
      'accountholdername': new FormControl('', [Validators.required]),
      'ifsccode': new FormControl('', [Validators.required]),

      'gender': new FormControl('', [Validators.required]),
      'role': new FormControl('', [Validators.required]),
      'mobilenum': new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]),

    });
    this.userService.getCountryList().subscribe(data => {
      this.countries = data.data;
    })

  }

  checkAadhar(data) {

    this.aadharnumber = { aadharnumber: data };
    this.userService.checkData(this.aadharnumber).subscribe((data) => {
      this.aadhar = data.isExist;

    });
  }
  checkHardwareid(data) {
    this.gateway = { "gateway": data };
    this.userService.checkData(this.gateway).subscribe(data => {
      this.hwid = data.isExist;
    });
  }
  checkPan(data) {
    this.pannumber = { "pannumber": data }
    this.userService.checkData(this.pannumber).subscribe(data => {
      this.pan = data.isExist;
    })
  }

  addPond() {
    alert("addPond function")
    jQuery("#pond_row").clone(true).insertAfter("#pond_row");
    jQuery("#pond_row").find("a").hide();
    return false;
    // this.userForm.addControl("pond_name_1",new FormControl('',[Validators.required]))
  }

  updateData(data) {
    let userData = {
      id: this.userId,
      data:data
    }
    this.userService.updateUserDetails(userData).subscribe(data => {
      this.updatedata = data;
      alert(JSON.stringify(this.updatedata));
    })
  }



  stateList() {
    this.userService.getStateList(this.country).subscribe(data => {
      this.states = data.data;

    })
  }

  getDistrict() {
    this.userService.getDistrictList(this.state).subscribe(data => {
      this.districts = data.data;
    })
  }
  getCity() {
    this.userService.getCityList(this.district).subscribe(data => {
      this.cities = data.data;

    })
  }


}
