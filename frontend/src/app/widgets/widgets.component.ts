import { Component, OnInit } from '@angular/core';
import { ChartService } from '../Services/chart.service';
import * as io from 'socket.io-client';


@Component({
  selector: 'app-widgets',
  templateUrl: './widgets.component.html',
  styleUrls: ['./widgets.component.css'],
  providers: [ChartService]
})
export class WidgetsComponent implements OnInit {
  gateway: any;
  gatewayid: any;
  private socket;

  constructor(private dashboardService: ChartService) { 
   // this.socket=io.connect('http://localhost:8080');
   

  }

  ngOnInit() {
    console.log(localStorage.getItem('hardware'))
    this.gatewayid = localStorage.getItem('hardware')
    this.dashboardService.getValuesFordashboard(this.gatewayid).subscribe((response) => {
      this.gateway = response.data[0];
      console.log(JSON.stringify(this.gateway));
      
    })
  }

}
