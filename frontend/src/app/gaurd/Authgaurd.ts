import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router) { }
    userData: any;
    userid: any;
    hardwareData:any;
    canActivate() {
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            console.log("entered into authGAurd")
            this.userData = JSON.parse(localStorage.getItem('currentUser'));
            console.log("entered into authGAurd"+JSON.stringify(this.userData))
            this.userid = this.userData.user_info.id;
            return true;
        }
        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    }



}
