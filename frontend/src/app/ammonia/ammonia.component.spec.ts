import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmmoniaComponent } from './ammonia.component';

describe('AmmoniaComponent', () => {
  let component: AmmoniaComponent;
  let fixture: ComponentFixture<AmmoniaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmmoniaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmmoniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
