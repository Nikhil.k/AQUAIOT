import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChartService } from '../../app/Services/chart.service';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
import { Observable } from 'rxjs/Rx';
import { AnonymousSubscription } from 'rxjs/Subscription'
import 'rxjs/add/operator/map';
import * as Plotly from 'plotly.js/lib/core';
import { Constants } from '../constants/constants';
import {AuthGuard} from '../gaurd/Authgaurd';
import { AquaService } from '../Services/login.service';
import { ScatterData, Layout, PlotlyHTMLElement, newPlot } from 'plotly.js/lib/core';


@Component({
  selector: 'app-ammonia',
  templateUrl: './ammonia.component.html',
  styleUrls: ['./ammonia.component.css'],
  providers: [ChartService, AmChartsService,Constants,AquaService]
})

export class AmmoniaComponent implements OnInit {

  ammoniaSubscription: AnonymousSubscription;
  timerSubscription: AnonymousSubscription;

  gatewayid: any;
  pHData = [];
  AmmoniaData = [];
  NitrateData = [];
  dissolvedData = [];
  dashboardData: any;
  timeStamps = [];
  graphDiv = '#Ammonia';
  chart: any;
  sendOtp;

  public options: any;
  private chart2: AmChart;
  time: Number;
  public dataProvider = [];

  constructor(private dashboard: ChartService, private amchart: AmChartsService,private constant: Constants, private authgaurd: AuthGuard, private loginService: AquaService) { }

  ngOnInit() {
    //  this.gatewayid=localStorage.getItem('hardware');
    console.log(localStorage.getItem('hardware'));
    this.gatewayid = localStorage.getItem('hardware');
    this.refreshData();

  }
  subscribeToData(): void {
    this.ammoniaSubscription = Observable.timer(5000).first().subscribe(() => this.refreshData())
  }
  refreshData(): void {
    this.dashboard.getChartValues(this.gatewayid).subscribe((response) => {
      this.dashboardData = response.data;
      this.sendOtp = 0;
      for (let i = 0; i <= this.dashboardData.length; i++) {
        this.sendOtp = this.sendOtp == 0 ? this.dashboardData[i].Ammonia : this.sendOtp;
        if (this.dashboardData[i].Ammonia <= this.sendOtp) {
          this.sendOtp = this.dashboardData[i].Ammonia;
        }
        this.AmmoniaData.push(this.dashboardData[i].Ammonia);
        let date = this.dashboard.getDate(this.dashboardData[i].timestamps);
        this.timeStamps.push(date);
      }
    })

    if (this.sendOtp >= this.constant.AMMONIA_MIN  && this.sendOtp!=undefined && this.sendOtp!="") {
      //  alert('OTP sent successfully to :-----'+this.authgaurd.userData.user_info.firstname)

      let data = {
        templateName: 'AquaValuesTemplate',
        username: this.authgaurd.userData.user_info.firstname,
        mobileNumber:this.authgaurd.userData.user_info.mobilenum,
        currentLevel: 'Ammonia ' + this.sendOtp,
        actualLevel: 'Ammonia ' + this.constant.AMMONIA_MIN
      }
    //  alert('OTP sent successfully with below data' + JSON.stringify(data))
      //this.loginService.sendOtp()
      this.loginService.sendOtp(data).subscribe((result)=>{
        console.log('OTP sent to registered mobile number successfully')
      })
    }
    this.subscribeToData();
  }

  ngAfterViewInit() {
    const trace2 = {
      x: this.timeStamps,
      y: this.AmmoniaData,
      name: 'Ammonia',
      type: 'scatter'
    } as ScatterData;
    const data = [trace2];
    const layout = {
      title: 'Ammonia Values',
      xaxis: {
        title: '----Time----',
        showgrid: false,
        zeroline: false
      },
      yaxis: {
        title: 'Values',
        showline: false
      },

    };
    //displayModeBar: false
    // staticPlot: true
    // showLink: True
    Plotly.newPlot(this.graphDiv, data, layout, { displayModeBar: false });

  }


}
