import { Component, OnInit } from '@angular/core';
import { GatewayService } from '../../app/Services/gateway.service';
import { AuthGuard } from '../gaurd/Authgaurd';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component'


@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.css'],
  providers: [GatewayService]
})
export class GatewayComponent implements OnInit {
  gatewayData: any;
  data: any;
  myid: any;
  hardwareid: any;
  gatewayDetails = [];
  constructor(private gatewayService: GatewayService, private AuthGuard: AuthGuard, private router: Router, private appComponent: AppComponent) { }

  ngOnInit() {
    this.myid = this.AuthGuard.userData.user_info.id
    this.gatewayService.getGateways(this.myid).subscribe((response) => {
      this.gatewayData = response.data
      for (let i = 0; i < this.gatewayData[0].ponddtls.length; i++) {
        this.gatewayDetails.push(this.gatewayData[0].ponddtls[i].hardwareid)
      }
    })
  }

  getPonddtls(value) {
    
    this.router.navigate(['dashboard'], { queryParams: { gateway: value } })
  }


}
