import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { AppComponent } from '../app.component'
import { AquaService } from '../../app/Services/login.service';
import { GatewayService } from '../../app/Services/gateway.service';
// import { GatewayComponent } from '../gateway/gateway.component';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { setTimeout } from 'timers';
// import { addListener } from 'cluster';
declare var $: any;


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [AquaService, GatewayService],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    rForm = {};
    loading = false;
    error = '';
    model: any = {};
    userInfo: any;
    gatewayData: any;
    registrationForm = {};
    gatewayDetails = [];
    registeredData = {};
    otp: any;
    newpassword:any;
    mobilenumber: any;
    confirmpassword:any;
    verifyotp: any;
    mobNumber: any;
    moblNumber:any;
    verifyOtppass:any;
    verifyOtpno: boolean;
    verify:any;
    passwordmatch:boolean=false;
    isMobile: boolean = false;
    constructor(private router: Router, private aquaservice: AquaService, private fb: FormBuilder, private http: Http, private gateway: GatewayService, private appcomponent: AppComponent) {
        this.rForm = fb.group({
            'mobilenum': [null, [Validators.required, Validators.pattern('[0-9]+'),
            Validators.minLength(10), Validators.maxLength(10)]],
            'password': [null, Validators.required]
        });
        this.registrationForm = fb.group({
            "firstname": [null, Validators.required],
            "lastname": [null, Validators.required],
            'email': ['', [Validators.pattern('^[a-z0-9!#$%&{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$')]],
            "mobilenum": [null, [Validators.required, Validators.pattern('[0-9]+'),
            Validators.minLength(10), Validators.maxLength(10)]],
        })
    }

    ngOnInit() {
    }

    "gen": any[] = [
        { id: 'M', gender: 'Male' },
        { id: 'F', gender: 'Female' }
    ];
    isSuccess = true;


    isEmailHide = true;
    validUniqueEmail(email) {
        this.aquaservice.validUniqueEmail(email).subscribe((result) => {
            console.log(result);
            this.isEmailHide = true;
            if (result.data == true) {
                this.isEmailHide = false;
            }
            if (result.data == false || this.isEmailHide == true) {
                this.isEmailHide = true;
            }
        })
    }
    isMobileHide = true;
    validMobileNumber(number) {
        this.aquaservice.validUniqueMobile(number).subscribe((result) => {
            console.log(result);
            this.isMobileHide = true;
            if (result.data == true) {
                this.isMobileHide = false;
            }
            if (result.data == false || this.isMobileHide == true) {
                this.isMobileHide = true;
            }
        })
    }


    signIn(post) {
        this.aquaservice.login(post).subscribe((result => {
            console.log(result);
            this.aquaservice.token = result;
            console.log(this.aquaservice.token);
            localStorage.setItem('currentUser', JSON.stringify(this.aquaservice.token));
            console.log(localStorage.getItem('currentUser'))
            this.aquaservice.setToken(result);
            this.aquaservice.isAuthenticated().subscribe((result1) => {
                this.userInfo = result1.user_info
                localStorage.setItem('currentUser', JSON.stringify(result1));
                if (this.userInfo.rolemstid == 2) {
                    this.gateway.getGateways(this.userInfo.id).subscribe((response) => {
                        this.gatewayData = response.data
                        for (let i = 0; i < this.gatewayData[0].ponddtls.length; i++) {
                            this.gatewayDetails.push(this.gatewayData[0].ponddtls[i].hardwareid)

                        }
                        if (this.gatewayDetails.length > 1) {
                            this.router.navigate(['gateway'])
                        }
                        else {
                            alert("login component" + JSON.stringify(this.gatewayDetails[0]))
                            console.log(this.userInfo.id);
                            this.router.navigate(['dashboard'], { queryParams: { gateway: this.gatewayDetails[0] } });
                        }
                    })
                }
                else {
                    console.log(this.userInfo.rolemstid)
                    this.router.navigate(['dashboard/viewuser'])
                }
            })
        }))
    }


    sendOtp(data) {
        this.registeredData = data;
        this.mobNumber = data.mobilenum.toString().slice(-4);
        this.aquaservice.sendOtp({ templateName: "AquaTemplate", mobileNumber: data.mobilenum }).subscribe((response) => {
            this.otp = response.otpNumber;
            $("#otpdiv").show();
            $('#registartiondiv').hide();
        })
    }

    registerData() {
        $('#afterOtp').hide();

        console.log("otp" + this.otp)
        this.verifyOtpno = false;
        if (this.otp == this.verifyotp) {
            this.aquaservice.registerUser(this.registeredData).subscribe((result) => {
                $("#successdiv").fadeIn(300);
                this.verifyOtpno = false;
                setTimeout(() => {
                    $("#successdiv").fadeOut(400);
                    $('#otpdiv').hide();
                    $('#otp').hide();
                    // $('#exampleModal').hide();
                    // $('.modal').hide();
                    $('.fade').hide();
                    // $('.modal').fadeOut();
                }, 3000);

            })
        }
        else {
            this.verifyOtpno = true;
        }
    }

    checkmobilenum(number) {

        this.aquaservice.validUniqueMobile(number).subscribe((result) => {
            console.log(result);
            this.isMobile = true
            if (result.data == true) {
                this.isMobile = false;
            }
            if (result.data == false) {
                this.isMobile = true;
            }
        })

    }
    resetPassword() {
        this.moblNumber=this.mobilenumber.toString().slice(-4);
        this.aquaservice.resetPassword({ mobile: this.mobilenumber }).subscribe((response) => {
            this.verifyOtppass=response.data1.otpNumber
              $('#forgotdiv').hide();
              $('#forgotpdiv').show();
        })
    }

    verifyOtpforPassword(){
        console.log("At line 195----"+this.verifyOtppass)
             if(this.verifyOtppass==this.verify){
                  $('#forgotpdiv').hide();
                  $('#updatediv').show();
            }   
             else{
                this.verifyOtpno = true;
             }
    }

    updatePassword(){
        $('#forgotpdiv').hide();
        $('#forgotdiv').hide();
       
        if(this.newpassword==this.confirmpassword){
            this.aquaservice.updatePassword({mobilenum:this.mobilenumber,newPassword:this.newpassword}).subscribe((result)=>{
                $("#updatesuccessdiv").fadeIn(300);
                this.verifyOtpno = false;
                setTimeout(() => {  
                    $("#successdiv").fadeOut(400);
                    $('#updatediv').hide();
                    $('#otp').hide();
                    // $('#exampleModal').hide();
                    // $('.modal').hide();
                    $('.fade').hide();
                    // $('.modal').fadeOut();
                }, 3000);
            })

        }
        else{
            this.passwordmatch=true
            $('#forgotpdiv').hide();
            $('#forgotdiv').hide();
        }
       
    }

}
