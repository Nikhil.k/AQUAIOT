import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DissolvedoxygenComponent } from './dissolvedoxygen.component';

describe('DissolvedoxygenComponent', () => {
  let component: DissolvedoxygenComponent;
  let fixture: ComponentFixture<DissolvedoxygenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DissolvedoxygenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DissolvedoxygenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
