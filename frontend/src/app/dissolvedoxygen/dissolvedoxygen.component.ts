import { Component, OnInit } from '@angular/core';
import { ChartService } from '../../app/Services/chart.service';
import * as Plotly from 'plotly.js/lib/core';
import { Observable } from 'rxjs/Rx';
import { AnonymousSubscription } from 'rxjs/subscription';
import { AquaService } from '../Services/login.service';
import { Constants } from '../constants/constants';
import { AuthGuard } from '../gaurd/Authgaurd';
import { ScatterData, Layout, PlotlyHTMLElement, newPlot } from 'plotly.js/lib/core';

@Component({
  selector: 'app-dissolvedoxygen',
  templateUrl: './dissolvedoxygen.component.html',
  styleUrls: ['./dissolvedoxygen.component.css'],
  providers: [ChartService, Constants, AquaService]

})
export class DissolvedoxygenComponent implements OnInit {
  timer: AnonymousSubscription;
  gatewayid: any;
  dissolvedData = [];
  dashboardData: any;
  timeStamps = [];
  graphDiv = '#DO';
  sendOtp;

  constructor(private dashboard: ChartService, private constant: Constants, private authgaurd: AuthGuard, private loginService: AquaService) { }

  ngOnInit() {
    //  this.gatewayid=localStorage.getItem('hardware');
    console.log(localStorage.getItem('hardware'));
    this.gatewayid = localStorage.getItem('hardware');
    this.refreshData();

  }
  subscribeToData(): void {
    this.timer = Observable.timer(5000).first().subscribe(() => this.refreshData())
  }
  refreshData(): void {
    this.dashboard.getChartValues(this.gatewayid).subscribe((response) => {
      this.dashboardData = response.data;
      this.sendOtp = 0;

      for (let i = 0; i <= this.dashboardData.length; i++) {
        this.sendOtp = this.sendOtp == 0 ? this.dashboardData[i].PH : this.sendOtp;

        this.dissolvedData.push(this.dashboardData[i].dissolvedoxygen);
        let date = this.dashboard.getDate(this.dashboardData[i].timestamps);
        this.timeStamps.push(date);
      }
    })
    this.subscribeToData();
    if ((this.sendOtp <= this.constant.DISSOLVEDOXYGEN_MIN || this.sendOtp <= this.constant.DISSOLVEDOXYGEN_MAX) && this.sendOtp!=undefined && this.sendOtp!="") {
      //  alert('OTP sent successfully to :-----'+this.authgaurd.userData.user_info.firstname)

      let data = {
        templateName: 'AquaValuesTemplate',
        username: this.authgaurd.userData.user_info.firstname,
        mobileNumber:this.authgaurd.userData.user_info.mobilenum,
        currentLevel: 'Dissolved Oxygen' + this.sendOtp,
        actualLevel: 'Dissolved Oxygen' + this.constant.DISSOLVEDOXYGEN_MIN
      }
    //  alert('OTP sent successfully with below data' + JSON.stringify(data))
      //this.loginService.sendOtp()
      this.loginService.sendOtp(data).subscribe((result)=>{
        console.log('OTP sent to registered mobile number successfully')
      })
    }
  }

  ngAfterViewInit() {
    const trace2 = {
      x: this.timeStamps,
      y: this.dissolvedData,
      name: 'DissolvedOxygen',
      type: 'scatter'
    } as ScatterData;
    const data = [trace2];
    const layout = {
      title: 'Dissolved Oxygen Values',
      xaxis: {
        title: '----Time----',
        showgrid: false,
        zeroline: false
      },
      yaxis: {
        title: 'Values',
        showline: false
      },

    };
    //displayModeBar: false
    // staticPlot: true
    // showLink: True
    Plotly.newPlot(this.graphDiv, data, layout, { displayModeBar: false });

  }


}
