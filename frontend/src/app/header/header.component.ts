import { Component, OnInit } from '@angular/core';
import {AquaService} from '../Services/login.service'
import {Router} from '@angular/router'
import { AuthGuard } from '../gaurd/Authgaurd';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  providers:[AquaService]
})
export class HeaderComponent implements OnInit {
   fname:any;
   lname:any;
   email:any;
  constructor(private aquaservice:AquaService,private router:Router,private AuthGuard:AuthGuard) { }

  ngOnInit() {
    this.fname=this.AuthGuard.userData.user_info.firstname
    this.lname=this.AuthGuard.userData.user_info.lastname
    this.email=this.AuthGuard.userData.user_info.email
    
    
  }
  logout(){
    console.log(">>." + JSON.stringify(localStorage.getItem('currentUser')));
    this.aquaservice.logout();
    this.router.navigate(['login']);
    console.log("navigated");
  
  }
}


