import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { PhComponent } from './ph/ph.component';
import { AmmoniaComponent } from './ammonia/ammonia.component';
import { NitrateComponent } from './nitrate/nitrate.component';
import { AuthGuard } from './gaurd/Authgaurd';
import { DissolvedoxygenComponent } from './dissolvedoxygen/dissolvedoxygen.component';
import { WidgetsComponent } from './widgets/widgets.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { BrowserAnimationBuilder } from '@angular/platform-browser/animations/src/animation_builder';
import { GatewayComponent } from './gateway/gateway.component';
import { CreateuserComponent } from './createuser/createuser.component';
import { ViewuserComponent } from './viewuser/viewuser.component';
import { TechnicianComponent } from './technician/technician.component';

const approutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },

  { path: 'gateway', component: GatewayComponent, canActivate: [AuthGuard] },

  {
    path: 'dashboard', canActivate: [AuthGuard], children: [
      { path: '', component: DashboardComponent },
      { path: 'pH', component: PhComponent },
      { path: 'Ammonia', component: AmmoniaComponent },
      { path: 'Nitrate', component: NitrateComponent },
      { path: 'DO', component: DissolvedoxygenComponent },
      { path: 'createuser', component: CreateuserComponent },
      { path: 'viewuser', component: ViewuserComponent }, 
      { path: 'ponddetails', component: TechnicianComponent },         
      { path: '', component: SidemenuComponent, outlet: 'sidemenu' },
      { path: '', component: HeaderComponent, outlet: 'header' },
      { path: '', component: FooterComponent, outlet: 'footer' }
    ],
  }]
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidemenuComponent,
    PhComponent,
    AmmoniaComponent,
    NitrateComponent,
    DissolvedoxygenComponent,
    WidgetsComponent,
    DashboardComponent,
    FooterComponent,
    LoginComponent,
    GatewayComponent,
    CreateuserComponent,
    ViewuserComponent,
	TechnicianComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    AmChartsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(approutes)
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
