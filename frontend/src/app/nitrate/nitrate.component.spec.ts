import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NitrateComponent } from './nitrate.component';

describe('NitrateComponent', () => {
  let component: NitrateComponent;
  let fixture: ComponentFixture<NitrateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NitrateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NitrateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
