import { Component, OnInit } from '@angular/core';
import { ChartService } from '../../app/Services/chart.service';
import * as Plotly from 'plotly.js/lib/core';
import { Observable } from 'rxjs/Rx';
import { AnonymousSubscription } from 'rxjs/Subscription';
import { AquaService } from '../Services/login.service'
import { ScatterData, Layout, PlotlyHTMLElement, newPlot } from 'plotly.js/lib/core';
import { Constants } from '../constants/constants';
import {AuthGuard} from '../gaurd/Authgaurd';


@Component({
  selector: 'app-nitrate',
  templateUrl: './nitrate.component.html',
  styleUrls: ['./nitrate.component.css'],
  providers: [ChartService, AquaService,Constants]

})
export class NitrateComponent implements OnInit {
  nitrateSubscription: AnonymousSubscription;
  gatewayid: any;
  NitrateData = [];
  dashboardData: any;
  timeStamps = [];
  graphDiv = '#nitrate';
  sendOtp;
  constructor(private dashboard: ChartService,private loginService:AquaService,private constant:Constants,private authgaurd:AuthGuard) { }
  ngOnInit() {
    //  this.gatewayid=localStorage.getItem('hardware');
    console.log(localStorage.getItem('hardware'));
    this.gatewayid = localStorage.getItem('hardware');
    this.refreshData();
  }
  subscribeToData(): void {
    this.nitrateSubscription = Observable.timer(5000).first().subscribe(() => this.refreshData())
  }
  refreshData(): void {
    this.dashboard.getChartValues(this.gatewayid).subscribe((response) => {
      this.dashboardData = response.data;
      this.sendOtp=0;
      for (let i = 0; i <= this.dashboardData.length; i++) {
        this.sendOtp = this.sendOtp == 0 ? this.dashboardData[i].Nitrate : this.sendOtp;
        if (this.dashboardData[i].Nitrate <= this.sendOtp) {
          this.sendOtp = this.dashboardData[i].Nitrate;
        }
        this.NitrateData.push(this.dashboardData[i].Nitrate);
        let date = this.dashboard.getDate(this.dashboardData[i].timestamps);
        this.timeStamps.push(date);
      }
    })
    if (this.sendOtp >= this.constant.NITRATE_MIN  && this.sendOtp!=undefined && this.sendOtp!="") {

      let data = {
        templateName: 'AquaValuesTemplate',
        username: this.authgaurd.userData.user_info.firstname,
        mobileNumber:this.authgaurd.userData.user_info.mobilenum,
        currentLevel: 'Nitrate ' + this.sendOtp,
        actualLevel: 'Nitrate ' + this.constant.NITRATE_MIN
      }

      this.loginService.sendOtp(data).subscribe((result)=>{
        console.log('OTP sent to registered mobile number successfully')
      })
    }
    this.subscribeToData();
  }

  ngAfterViewInit() {
    const trace2 = {
      x: this.timeStamps,
      y: this.NitrateData,
      name: 'Nitrate',
      type: 'scatter'
    } as ScatterData;
    const data = [trace2];
    const layout = {
      title: 'Nitrate Values',
      xaxis: {
        title: '----Time----',
        showgrid: false,
        zeroline: false
      },
      yaxis: {
        title: 'Values',
        showline: false
      },

    };
    //displayModeBar: false
    // staticPlot: true
    // showLink: True
    Plotly.newPlot(this.graphDiv, data, layout, { displayModeBar: false });

  }

}
