import { Component, OnInit } from '@angular/core';
import { UserService } from '../Services/user.service';
import { AuthGuard } from '../gaurd/Authgaurd';
import { Router } from '@angular/router';



@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css'],
  providers: [UserService]
})
export class ViewuserComponent implements OnInit {
  registeredUsers: any;
  role: any;
  pendingPonds: any;
  isSuccess: boolean = false;

  constructor(private userService: UserService, private authgaurd: AuthGuard, private route: Router) { }

  ngOnInit() {
    this.role = this.authgaurd.userData.user_info.rolemstid;
    this.userService.getPendingPondDetails().subscribe((response) => {
      this.pendingPonds = response.data;
    })
  }

  isEditable = false;
  isUpdatable = false;
  isDelete = false;
  isRejected = false;
  isAction = false;

  getUsers(status) {
    if (status === "WaitingForCall") {
      this.isEditable = false;
      this.isUpdatable = true;
      this.isDelete = false;
      this.isRejected = false;
      this.isAction = false;

    }
    if (status === "Created") {
      this.isEditable = true;
      this.isUpdatable = false;
      this.isDelete = false;
      this.isRejected = false;
      this.isAction = false;

    }
    if (status === "Approved") {
      this.isEditable = false;
      this.isUpdatable = false;
      this.isDelete = true;
      this.isRejected = false;
      this.isAction = false;

    }
    if (status === "Rejected") {
      this.isEditable = false;
      this.isUpdatable = false;
      this.isDelete = false;
      this.isRejected = true;
      this.isAction = false;

    }
    if (status === "WaitingForPondDetails") {
      this.isAction = true;
    }

    this.userService.getUsers(status).subscribe((result) => {
      this.registeredUsers = result.data;
    })
  }



  updateUser(user) {
    this.userService.updateUserDetails(user).subscribe((result) => {
      setTimeout(() => {    //<<<---    using ()=> syntax
        this.isSuccess = false;
      }, 3000);
    })
  }

  isEmailHide = true;
  validUniqueEmail(email) {
    this.userService.validUniqueEmail(email).subscribe((result) => {
      console.log(result);
      this.isEmailHide = true;
      if (result.data == true) {
        this.isEmailHide = false;
      }
      if (result.data == false || this.isEmailHide == true) {
        this.isEmailHide = true;
      }
    })
  }
  isMobileHide = true;
  validMobileNumber(number) {
    this.userService.validUniqueMobile(number).subscribe((result) => {
      console.log(result);
      this.isMobileHide = true;
      if (result.data == true) {
        this.isMobileHide = false;
      }
      if (result.data == false || this.isMobileHide == true) {
        this.isMobileHide = true;
      }
    })
  }

  savePondDetails(id) {
    this.route.navigate(['dashboard/ponddetails'], { queryParams: { userId: id } })
  }

  editUser(userid) {
    console.log("edit user in viewuser component" + userid);
    this.route.navigate(['dashboard/createuser'],{queryParams:{usermstid:userid}})
  }

}
