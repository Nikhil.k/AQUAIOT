import { Component, OnInit } from '@angular/core';
import { AuthGuard } from '../gaurd/Authgaurd';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css']
})
export class SidemenuComponent implements OnInit {
   fname:any;
   lname:any;
   role:any;
   email:any;
  constructor(private AuthGuard:AuthGuard) { }

  ngOnInit() {
    
    this.fname=this.AuthGuard.userData.user_info.firstname
    this.lname=this.AuthGuard.userData.user_info.lastname
    this.role=this.AuthGuard.userData.user_info.rolemstid    
       
  }

}
