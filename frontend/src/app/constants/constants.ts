// module.exports.STATUS_CREATED = 'CREATED';
// module.exports.STATUS_CONFIRM = 'CONFIRM';
// module.exports.STATUS_REJECTED = 'REJECTED';
// module.exports.STATUS_PROCESSED = 'PROCESSED';



/**
 * for AquaValues
 *  
 */
// module.exports={'PH_MIN' : 6.5};   // A - ACTIVE
// module.exports={'PH_MAX' : 9.0}; // X-INACTIVE

export class Constants{
     PH_MIN :number =6.5;
     PH_MAX :number=9.0;
     AMMONIA_MIN:number=1.0;
     NITRATE_MIN:number=5.0;
     DISSOLVEDOXYGEN_MIN:number= 4;
     DISSOLVEDOXYGEN_MAX:number = 5;
}
// module.exports.AMMONIA_MIN = 1;
// module.exports.AMMONIA_MAX = 2;
// module.exports.NITRATE_MIN = 3;
// module.exports.NITRATE_MAX = 4;
// module.exports.DISSOLVEDOXYGEN_MIN = 5;
// module.exports.DISSOLVEDOXYGEN_MAX = 6;
// module.exports.PH_MIN = 7;
// module.exports.PH_MAX = 12;
// module.exports.PH_MIN = 14;