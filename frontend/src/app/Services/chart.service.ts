import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import  'rxjs/add/operator/map';



@Injectable()
export class ChartService {
    setId: any;

  constructor(private  http:Http) { }
   getValuesFordashboard(data){
       return this.http.get("http://localhost:8080/aqua/api/AquaValuesRoutes/getValuesForDashboard/"+data).map(res=>res.json())
   }
   getChartValues(data){
    return this.http.get("http://localhost:8080/aqua/api/AquaValuesRoutes/getAllDetails/"+data).map(res=>res.json())   }
    
    getDate(date){
        var x = new Date(date).toUTCString();
        var datea= x.slice(8, 12) +x.slice(5, 7) + "  " + x.slice(17, 22) 
        //  document.getElementById("demo").innerHTML = datea;
        return datea
        }

}
