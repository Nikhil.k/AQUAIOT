import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
// import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AuthGuard } from '../gaurd/Authgaurd';
import 'rxjs/add/operator/map';

@Injectable()

export class UserService {
    public token: string;
    public options: any;
    constructor(private http: Http, private auth: AuthGuard) { }

    getUsers(status) {
        console.log(status);
        console.log('/aqua/api/user/getUserDetails', status);
        let statu = { "status": status }
        return this.http.post('http://localhost:8080/aqua/api/user/getUserDetails', statu)
            .map((response: Response) => response.json())
    }

    updateUserDetails(data) {
        // alert(data);
        return this.http.post('http://localhost:8080/aqua/api/user/updateUserDetails', data)
            .map((response: Response) => response.json())
    }

    validUniqueEmail(email) {
        console.log(JSON.stringify(email));
        console.log('/aqua/api/user/uniqueEmail', email);
        return this.http.post('http://localhost:8080/aqua/api/user/uniqueEmail', email)
            .map((response: Response) => response.json());
    }

    validUniqueMobile(mobilenum) {
        return this.http.post('http://localhost:8080/aqua/api/user/uniqueMobile', mobilenum)
            .map((response: Response) => response.json());
    }

    checkData(data) {
        return this.http.post("http://localhost:8080/aqua/api/user/dataExistOrNot", data)
            .map(res => res.json())
    }
    savePondDetails(data) {
        console.log(JSON.stringify(data));
        return this.http.post("http://localhost:8080/aqua/api/pond/savePondDetails", data) 
            .map(res => res.json())
    }

    getCountryList() {
        return this.http.get("http://localhost:8080/aqua/api/user/getCountryList")
            .map(res => res.json())
    }

    getStateList(id) {
        return this.http.get("http://localhost:8080/aqua/api/user/getStatesByCountry/" + id)
            .map(res => res.json())
    }
    getDistrictList(id) {
        return this.http.get("http://localhost:8080/aqua/api/user/getDistrictsByState/" + id)
            .map(res => res.json())
    }
    getCityList(id) {
        return this.http.get("http://localhost:8080/aqua/api/user/getCitiesByState/" + id)
            .map(res => res.json())
    }

    getUserById(userID) {
        return this.http.get('http://localhost:8080/aqua/api/user/getUserById/' + userID)
            .map((response: Response) => response.json())
    }

    getPendingPondDetails() {
        return this.http.get('http://localhost:8080/aqua/api/pond/getPendingPondDetails')
            .map((response: Response) => response.json())
    }
}
