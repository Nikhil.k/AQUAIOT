import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import "rxjs/add/operator/map"

@Injectable()
export class GatewayService {

  constructor(private http: Http) { }

  getGateways(data) {
    return this.http.get("http://localhost:8080/aqua/api/pond/getGatewayDetailsByUserid/" + data).map(res=>res.json()  
  )
  }

}
