import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
// import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { AuthGuard } from '../gaurd/Authgaurd';
import 'rxjs/add/operator/map';

@Injectable()

export class AquaService {
    public token: string;
    public options: any;
    constructor(private http: Http, private auth: AuthGuard) { }

    createAuthorizationHeader(headers: Headers) {
        console.log(headers);
        headers.append('Authorization', 'Basic ' +
            btoa('username:password'));
        console.log(headers);
    }

    login(data) {
        return this.http.post('http://localhost:8080/aqua/api/token', data)
            .map((response: Response) => response.json());
    }

    setToken(token) {
        console.log(JSON.stringify(token.token).toString().replace(/"/g, ""));
        let headers = new Headers({ 'Authorization': 'JWT ' + JSON.stringify(token.token).toString().replace(/"/g, "") });
        this.options = new RequestOptions({ headers: headers });
        console.log(JSON.stringify(headers) + " >>" + JSON.stringify(this.options));
    }

    isAuthenticated() {
        console.log(this.options);
        return this.http.get('http://localhost:8080/aqua/api/me', this.options)
            .map((response: Response) => response.json())
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        // this.token = null;
        localStorage.removeItem('currentUser');
        console.log("after removeItem");
    }

    registerUser(userData) {
        console.log(userData);
        return this.http.post('http://localhost:8080/aqua/api/user/registerUserDetails', userData)
            .map((response: Response) => { response.json(), console.log(response); });
    }

    validUniqueEmail(email) {
        console.log(JSON.stringify(email));
        console.log('/aqua/api/user/uniqueEmail', email.email);
        return this.http.post('http://localhost:8080/aqua/api/user/uniqueEmail', email)
            .map((response: Response) => response.json());
    }

    validUniqueMobile(mobilenum) {
        return this.http.post('http://localhost:8080/aqua/api/user/uniqueMobile', mobilenum)
            .map((response: Response) => response.json());
    }

    sendOtp(data) {
        return this.http.post('http://localhost:8080/aqua/api/user/sendOtp', data)
            .map((response: Response) => response.json());
    }

    resetPassword(data){
        return this.http.post('http://localhost:8080/aqua/api/user/resetPassword', data)
        .map((response: Response) => response.json());
    }

    updatePassword(data){
        return this.http.post('http://localhost:8080/aqua/api/user/updatePassword', data)
        .map((response: Response) => response.json());
    }
}
